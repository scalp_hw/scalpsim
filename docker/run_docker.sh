#!/bin/bash

# Configuration
IMG_NAME="scalpsim"
IMG_VERSION="latest"
MNT_FOLDER="/usr/scalpsim"

# Move to .. folder to avoid problems with build context. Reference:
# https://www.jamestharpe.com/include-files-outside-docker-build-context/
CURR_DIR=$PWD
cd ..

# Build scalpsim image (if needed)
if [[ "$(docker images -q $IMG_NAME:$IMG_VERSION 2> /dev/null)" == "" ]]; then
    docker build -t $IMG_NAME:$IMG_VERSION -f ${CURR_DIR}/Dockerfile .
fi

# Run scalpsim image (bash). Then execute scripts from inside
docker run -it \
    --user=$(id -u) \
    --env="DISPLAY" \
    --volume="/etc/group:/etc/group:ro" \
    --volume="/etc/passwd:/etc/passwd:ro" \
    --volume="/etc/shadow:/etc/shadow:ro" \
    --volume="/etc/sudoers.d:/etc/sudoers.d:ro" \
    --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" \
    --workdir=$MNT_FOLDER \
    --volume="$PWD":$MNT_FOLDER \
    $IMG_NAME:$IMG_VERSION

# Current folder mounted in $MNT_FOLDER

# Note: Options taken from https://stackoverflow.com/questions/46018102/how-can-i-use-matplotlib-pyplot-in-a-docker-container
# To allow matplotlib to plot from docker image

# Get back to previous folder
cd $CURR_DIR
