#! /usr/bin/python
"""
SCALP SOM simulator

For more information: python som_simulator.py --help

Possible parameters include:
    --seed: the random number generator seed. Ex: --seed=123
    --inputs: the of inputs to generate. Ex: --inputs=10000
    --saveplot: generate a gif plot at "output/som.gif".
        Matplotlob offers a limited memory for the gif, therefor keep the number of inputs low.
        Ex: --saveplot=True
    --plotlive: display live the generated plot during the simulation. Ex: --plotlive=True
"""

import click
from typing import List, Tuple, Callable
from itertools import islice
from time import sleep
from threading import Condition
from scalp import Network, random, statistics, perf
from scalp.router import unicast
from scalp.node.computingcore import som
from scalp.common import Direction, PositionVector
from scalp.inputs import  SadleGenerator
from scalp.visualizers import LiveWeights3dVisualizer
from scalp.node.computingcore.som.utils import euclidean_distance


def configure_interfaces():
    # throughput defined as fraction of message send per cycle (supposed message size: 32bytes)
    # latency defined in cycles
    fast_interface = perf.NIFConfig(throughput=1 / 32, latency=50)
    low_latency_interface = perf.NIFConfig(
        throughput=1 / 64, latency=10
    )  # half the throughput, a fifth of the latency
    noc_interface = perf.NIFConfig(
        throughput=1, latency=0
    )  # FPGA<->ComputingCore local communication

    perf.set_route_cost(5)  # cost in cycles for 1 routing operation
    perf.set_interface(Direction.EAST, fast_interface)
    perf.set_interface(Direction.WEST, fast_interface)
    perf.set_interface(Direction.NORTH, fast_interface)
    perf.set_interface(Direction.SOUTH, fast_interface)
    perf.set_interface(Direction.UP, low_latency_interface)
    perf.set_interface(Direction.DOWN, low_latency_interface)
    perf.set_interface(Direction.ComputingCore, noc_interface)
    perf.set_interface(Direction.FPGA, noc_interface)


def configure_som(
    eps_start: int,
    eps_end: int,
    sigma_start: int,
    sigma_end: int,
    inputslist: List[Tuple[float, ...]],
    side: int,
    on_iteration: Callable[[int], None]
):

    random_state = random.create_random_state()

    def rng():
        return random_state.random_sample()

    def initial_weight_2d(p: PositionVector):
        x, y, z = p

        return [x * 2 / side - 1, y * 2 / side - 1 , 0]

    som.KSom.config = som.KSomConfig(
        eps_start=eps_start,
        eps_end=eps_end,
        sigma_start=sigma_start,
        sigma_end=sigma_end,
        input_node=PositionVector(0, 0, 0),
        inputs=inputslist,
        iteration_cb=on_iteration,
        initial_weight=initial_weight_2d,
    )


@click.command()
@click.option("--inputs", type=click.INT)
@click.option("--side", type=click.INT)
@click.option("--eps_start", type=click.FLOAT)
@click.option("--eps_end", type=click.FLOAT)
@click.option("--sigma_start", type=click.FLOAT)
@click.option("--sigma_end", type=click.FLOAT)
@click.option("--live_aqe", default=False)
@click.option("--final_aqe", default=False)
def main(
    inputs,
    side,
    eps_start,
    eps_end,
    sigma_start,
    sigma_end,
    live_aqe,
    final_aqe,
):

    random.seed(1)
    configure_interfaces()

    network = None

    def find_winner(vector):
        return min(
            euclidean_distance(n.computingcore.weights, vector)
            for n in network.nodes.values()
        )

    def get_aqe():
        aqe = 0
        for i in range(inputs):
            aqe += pow(find_winner(inputslist[i]), 2)
        return aqe / inputs if inputs else 0

    animation_t = 0
    animation_last_input_vector = None
    animation_condition = Condition()
    animation_finished = Condition()


    def on_iteration(t, input_vector):
        if live_aqe:
            print(f"{t + 1}, {get_aqe()}")

        nonlocal animation_t, animation_last_input_vector
        animation_t = t + 1
        animation_last_input_vector = input_vector
        with animation_condition:
            animation_condition.notifyAll()
        with animation_finished:
           animation_finished.wait(1)

    configure_interfaces()

    input_generator = SadleGenerator()

    inputslist = list(islice(input_generator, inputs))
    learn_inputs = inputslist[:int(inputs)]
    configure_som(
        eps_start, eps_end, sigma_start, sigma_end, learn_inputs, side, on_iteration
    )

    network = Network(
        size=PositionVector(side, side, 1),
        computingcore_type=som.KSom,
        router_type=unicast.XyzRouter,
    )

    visualizer = LiveWeights3dVisualizer(
        title="Saddle equation: $z=x^2-y^2$",
        network=network,
        input_count=inputs,
        live=True,
        save="saddle2.gif"
    )

    import matplotlib.pyplot as plt
    for i in range(30*1):
        visualizer.update()

    network.simulate()

    while animation_t < inputs:
        with animation_condition:
            animation_condition.wait(1)
        visualizer.update(animation_last_input_vector, animation_t)
        with animation_finished:
           animation_finished.notifyAll()

    for i in range(30*5):
        visualizer.update()


    ended_ok = network.wait_end()

    visualizer.finish()

    if live_aqe:
        return
    aqe = get_aqe()

    if final_aqe:
        return print(aqe)
    if ended_ok:
        statistics.print_stats()
        # statistics.print_queues_state()
        # statistics.display_received_stats()
    else:
        statistics.print_error()

    plt.show()

if __name__ == "__main__":
    main()
