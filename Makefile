ROUTER=xyz
DIST=uniform
RATE=1024
SEED=1
LINKS=all_gtp


som_sadle_simulator:
	python som_sadle_simulator.py \
			--inputs=300 \
			--side=12 \
			--eps_start=1.4999542236328125 \
			--eps_end=0.39215087890625 \
			--sigma_start=1.0 \
			--sigma_end=0.125

3d_inputs_3dsom_400inputs:
	python som_3dinputs_simulator.py \
			--inputs=400 \
			--dimensions=3 \
			--side=4 \
			--eps_start=2.254325732588768 \
			--eps_end=0.1875 \
			--sigma_start=0.755859375 \
			--sigma_end=0.25


3d_inputs_2dsom_400inputs:
	python som_3dinputs_simulator.py \
			--inputs=400 \
			--dimensions=2 \
			--side=8 \
			--eps_start=1.4999542236328125 \
			--eps_end=0.39215087890625 \
			--sigma_start=1.0 \
			--sigma_end=0.125

3d_inputs_3dsom_100inputs:
	python som_3dinputs_simulator.py \
			--inputs=100 \
			--dimensions=3 \
			--side=4 \
			--eps_start=1.3347777637349623 \
			--eps_end=0.1802329275419244 \
			--sigma_start=1.2814453125 \
			--sigma_end=0.017057297800670338

3d_inputs_2dsom_100inputs:
	python som_3dinputs_simulator.py \
			--inputs=100 \
			--dimensions=2 \
			--side=8 \
			--eps_start=1.7797036849799497 \
			--eps_end=0.3549635570564333 \
			--sigma_start=1.2814453125 \
			--sigma_end=0.016053927341807377

find_3d_som_params:
	@python som_find_parameters.py \
			--inputs=100 \
			--dimensions=3 \
			--side=4 \
			--eps_start=1 \
			--eps_end=1 \
			--sigma_start=1 \
			--sigma_end=1


find_2d_som_params:
	@python som_find_parameters.py \
			--inputs=100 \
			--dimensions=2 \
			--side=8 \
			--eps_start=1 \
			--eps_end=1 \
			--sigma_start=1 \
			--sigma_end=1

find_3d_som_params_1000:
	@python som_find_parameters.py \
			--inputs=1000 \
			--learn_ratio=1 \
			--dimensions=3 \
			--side=4 \
			--eps_start=1 \
			--eps_end=1 \
			--sigma_start=1 \
			--sigma_end=1


find_2d_som_params_1000:
	@python som_find_parameters.py \
			--inputs=1000 \
			--learn_ratio=1 \
			--dimensions=2 \
			--side=8 \
			--eps_start=1 \
			--eps_end=1 \
			--sigma_start=1 \
			--sigma_end=1


som:
	python som_simulator.py --inputs=1000 --plotlive=True

unicast:
	python stress_test.py --computingcore=unicast --router=$(ROUTER) --dist=$(DIST) --rate=$(RATE) --seed=$(SEED) --links=$(LINKS) --csv=True

broadcast:
	python stress_test.py --computingcore=broadcast --router=$(ROUTER) --timeout=30 --seed=$(SEED) --links=$(LINKS) --csv=True

reduce:
	python stress_test.py --computingcore=reduce --router=$(ROUTER) --timeout=30  --seed=$(SEED) --links=$(LINKS) --csv=True

init:
	@python -m pip install -r requirements.txt

mypy:
# checks type decorations
	@mypy --ignore-missing-imports stress_test.py

pycycle:
# checks for circular dependencies
	@pycycle --here


.PHONY: init run mypy pycycle
