#! /usr/bin/python
"""
SCALP SOM simulator

For more information: python som_simulator.py --help

Possible parameters include:
    --seed: the random number generator seed. Ex: --seed=123
    --inputs: the of inputs to generate. Ex: --inputs=10000
    --saveplot: generate a gif plot at "output/som.gif".
        Matplotlob offers a limited memory for the gif, therefor keep the number of inputs low.
        Ex: --saveplot=True
    --plotlive: display live the generated plot during the simulation. Ex: --plotlive=True
"""

import logging
from typing import Callable
import click
from time import sleep
from threading import Condition
from time import sleep
import progressbar
from itertools import islice
from scalp import Network, random, statistics, perf
from scalp.router import unicast
from scalp.node.computingcore import stresstest, som
from scalp.node import distribution
from scalp.common import Direction, PositionVector, thread_yield
from scalp.inputs import Combine, ShapeGenerator
from scalp.visualizers import Weights2dVisualizer


def configure_interfaces():
    # throughput defined as fraction of message send per cycle (supposed message size: 32bytes)
    # latency defined in cycles
    fast_interface = perf.NIFConfig(throughput=1 / 32, latency=50)
    low_latency_interface = perf.NIFConfig(
        throughput=1 / 64, latency=10
    )  # half the throughput, a fifth of the latency
    noc_interface = perf.NIFConfig(
        throughput=1, latency=0
    )  # FPGA<->ComputingCore local communication

    perf.set_route_cost(5)  # cost in cycles for 1 routing operation
    perf.set_interface(Direction.EAST, fast_interface)
    perf.set_interface(Direction.WEST, fast_interface)
    perf.set_interface(Direction.NORTH, fast_interface)
    perf.set_interface(Direction.SOUTH, fast_interface)
    perf.set_interface(Direction.UP, low_latency_interface)
    perf.set_interface(Direction.DOWN, low_latency_interface)
    perf.set_interface(Direction.ComputingCore, noc_interface)
    perf.set_interface(Direction.FPGA, noc_interface)


def configure_som(inputs: int, on_iteration: Callable[[int], None], plot: bool):
    input_generator = Combine(
        generators=[
            ShapeGenerator([(1, 3), (1, 3)]),
            ShapeGenerator([(4, 6), (1, 3)]),
            ShapeGenerator([(1, 3), (4, 6)]),
            ShapeGenerator([(4, 6), (4, 6)]),
        ]
    )

    som.KSom.config = som.KSomConfig(
        eps_start=3.2813e-01,
        eps_end=3.2813e-03,
        sigma_start=8.7500e-01,
        sigma_end=8.7500e-03,
        input_node=PositionVector(0, 0, 0),
        inputs=list(islice(input_generator, inputs)),
        iteration_cb=on_iteration if plot else None,
        initial_weight=lambda p: [p[0], p[1]],
    )


@click.command()
@click.option("--seed", type=click.IntRange(1, 2 ** 32))
@click.option("--inputs", default=100)
@click.option("--saveplot", default=False)
@click.option("--plotlive", default=False)
def main(seed, inputs, saveplot, plotlive):
    SIDE = 8
    random.seed(1)
    configure_interfaces()
    animation_t = 0
    animation_last_input_vector = None
    animation_condition = Condition()
    plot = saveplot or plotlive

    def on_iteration(t, input_vector):
        nonlocal animation_t, animation_last_input_vector
        animation_t = t + 1
        animation_last_input_vector = input_vector
        with animation_condition:
            animation_condition.notifyAll()

    configure_som(inputs, on_iteration, plot)

    network = Network(
        size=PositionVector(SIDE, SIDE, 1),
        computingcore_type=som.KSom,
        router_type=unicast.XyzRouter,
    )

    if plot:
        visualizer = Weights2dVisualizer(
            network=network, input_count=inputs, live=plotlive, save="output/som.gif" if saveplot else None
        )

    network.simulate()

    if plot:
        with progressbar.ProgressBar(max_value=inputs) as bar:
            while animation_t < inputs:
                with animation_condition:
                    animation_condition.wait()
                visualizer.update(animation_last_input_vector, animation_t)
                bar.update(animation_t)

    ended_ok = network.wait_end()

    if plot:
        visualizer.finish()

    if ended_ok:
        print(", ".join(map(str, statistics.get_stats().values())))
        # visualizer.plot(block=True)
        # statistics.print_stats()
        # statistics.print_queues_state()
        # statistics.display_received_stats()
    else:
        statistics.print_error()


if __name__ == "__main__":
    main()
