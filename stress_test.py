#! /usr/bin/python
"""
Stress test simulator.

Simulates a SCALP architecture for testing different routers while benchmarking communication.
Arguments past to the simulation with determine the behavior of the simulation.

This script is used extensively by the performance benchmarking script measures/perf.sh

The computingcore param will determine all messages will be injected into the network:
    ex: --computingcore=unicast

The rate parameter will determine how many messages should be injected per second into the network
by all computing cores combined:
    ex: --rate=100

The router parameter specifies the unicast router to be used, usefull for the unicast computing core only:
    ex: --router=xyz

The dist parameter specifies what are the communication patterns for unicast communication.
    ex: --dist=uniform

The links parameter specifies which kind of links configuration is used.
    ex: --links=all_lvds

Is is possible to specify the size of the network by specifying its side so that size=side*3
    ex: --side=4

The number of dimensions can also be specified from the command-line
    ex: --numdim=3

When benchmarking, it is useful to seed the random numbers. This can be done using the seed parameter:
    ex: --seed=123

The simulation has no end, as message will continue be injected into the network at the specified rate.
Instead, we are interested in running the simulation for a fixed amount of time, and then observing the benchmarks.
This amount of time can be specified using the timeout parameter:
    ex: --timeout=15

The bechmark results are displayed in a human readable format.
One can use the --csv=True parameter to request a coma separated values printout of the results.

For more information, run: python stress_test.py --help

"""

import logging
import click
from scalp import Network, random, statistics
from scalp.perf import links
from scalp.router import unicast
from scalp.node.computingcore import stresstest
from scalp.node import distribution
from scalp.common import PositionVector, BroadcastMessage, \
                         ReduceOperator, ReduceMessage, CustomScheduler

logging.basicConfig(level=logging.INFO)


routers = {
    "xyz": unicast.XyzRouter,
    "xyz_adapt": unicast.XyzAdaptiveRouter,
    "adaptive": unicast.AdaptiveRouter,
    "virtual_channels": unicast.VirtualChannelRouter,
}

computingcores = {
    "unicast": stresstest.GenerateUnicastMessages,
    "broadcast": stresstest.GenerateBroadcastMessages,
    "reduce": stresstest.GenerateReduceMessages,
}

distributions = {
    "uniform": distribution.uniform,
    "complement": distribution.complement,
    "digitrevert": distribution.digitrevert,
    # Note: radius for sphere of locality used in 3D
    "sphere_of_locality": distribution.sphere_of_locality(3 ** (1 / 2), 0.9),
}

links_types = {
    "all_gtp": links.all_gtp,
    "all_lvds": links.all_lvds,
    "all_internal": links.all_internal,
    # Note: 3D routers
    "one_router_3d": links.one_router_3d,
    "eight_routers_3d": links.eight_routers_3d,
    # Note 2D routers
    "two_routers_2d": links.two_routers_2d,
}

@click.command()
@click.option(
    "--computingcore", required=True, type=click.Choice(computingcores.keys())
)
@click.option(
    "--router",
    required=True,
    type=click.Choice(routers.keys()),
)
@click.option(
    "--dist",
    type=click.Choice(distributions.keys()),
)
@click.option(
    "--links",
    required=True,
    type=click.Choice(links_types.keys()),
)
@click.option("--rate", type=click.IntRange(2, 2**32))
@click.option("--seed", type=click.IntRange(1, 2 ** 32))
@click.option("--side", default=4)
@click.option("--timeout", default=15)
@click.option("--csv", default=False)
@click.option("--numdim", default=3, type=click.IntRange(1, 3))
@click.option("--numcycles", default=1000)
def main(computingcore, router, dist, rate, seed, side, timeout, csv, links, numdim, numcycles):
    # Wait for all nodes every 5 cycles
    CustomScheduler.WAIT_ALL = 5

    links_types[links]()
    random.seed(seed)
    computingcoretype = computingcores[computingcore]
    if rate:
        # Minimum INTERVAL: 2
        computingcoretype.INTERVAL = rate
    if dist:
        computingcoretype.DISTRIBUTION = distributions[dist]

    network = Network(
        size=PositionVector(*( (numdim*[side]) + ((3-numdim)*[1]) )),
        router_type=routers[router],
        computingcore_type=computingcoretype,
        num_cycles=numcycles,
    )

    network.simulate()

    ended_ok = network.wait_end(timeout)
    if ended_ok:
        if csv:
            print(", ".join(map(str, [rate, *statistics.get_stats().values()])))
        else:
            statistics.print_stats()
        # statistics.display_final_stats(PositionVector(*( (numdim*[side]) + ((3-numdim)*[1]) )))
        # statistics.print_queues_state()
        # statistics.display_received_stats()
    else:
        statistics.print_error()


if __name__ == "__main__":
    main()
