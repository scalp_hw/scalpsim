#! /usr/bin/python
"""
Simulator for unicast messages.

Simulates a SCALP architecture for testing different routers while benchmarking communication.
Arguments past to the simulation with determine the behavior of the simulation.

This script is used extensively by the performance benchmarking script measures/perf.sh

The rate parameter will determine how many messages should be injected per second into the network
by all computing cores combined:
    ex: --rate=100

The router parameter specifies the unicast router to be used, usefull for the unicast computing core only:
    ex: --router=xyz

The dist parameter specifies what are the communication patterns for unicast communication.
    ex: --dist=uniform

The links parameter specifies which kind of links configuration is used.
    ex: --links=all_reference

Is is possible to specify the size of the network by specifying its side so that size=(size, size, 4)
    ex: --side=4

When benchmarking, it is useful to seed the random numbers. This can be done using the seed parameter:
    ex: --seed=123

The bechmark results are displayed in a human readable format.
One can use the --csv=True parameter to request a coma separated values printout of the results.

For more information, run: python unicast_gen.py --help

"""

import logging
import click
from scalp import Network, random, statistics
from scalp.perf import links
from scalp.router import unicast
from scalp.node.computingcore import stresstest
from scalp.node import distribution
from scalp.common import PositionVector, BroadcastMessage, \
                         ReduceOperator, ReduceMessage, CustomScheduler

logging.basicConfig(level=logging.INFO)


routers = {
    "xyz": unicast.XyzRouter,
    "xyz_adapt": unicast.XyzAdaptiveRouter,
    "adaptive": unicast.AdaptiveRouter,
}

distributions = {
    "uniform": distribution.uniform,
}

links_types = {
    "all_reference": links.all_reference,
    "eight_routers_3d": links.eight_routers_3d,
}

@click.command()
@click.option(
    "--router",
    required=True,
    type=click.Choice(routers.keys()),
)
@click.option(
    "--dist",
    type=click.Choice(distributions.keys()),
)
@click.option(
    "--links",
    required=True,
    type=click.Choice(links_types.keys()),
)
@click.option("--rate", type=click.IntRange(2, 2**32))
@click.option("--seed", type=click.IntRange(1, 2 ** 32))
@click.option("--side", default=4)
@click.option("--csv", default=False)
@click.option("--numcycles", default=1000)
def main(router, dist, rate, seed, side, csv, links, numcycles):
    # Wait for all nodes every 5 cycles
    CustomScheduler.WAIT_ALL = 5

    links_types[links]()
    random.seed(seed)
    if rate:
        # Minimum INTERVAL: 2
        stresstest.GenerateUnicastMessages.INTERVAL = rate
    if dist:
        stresstest.GenerateUnicastMessages.DISTRIBUTION = distributions[dist]

    network = Network(
        size=PositionVector(side, side, 4),
        router_type=routers[router],
        computingcore_type=stresstest.GenerateUnicastMessages,
        num_cycles=numcycles,
    )

    network.simulate()

    ended_ok = network.wait_end(None)
    if ended_ok:
        if csv:
            print(", ".join(map(str, [rate, *statistics.get_stats().values()])))
        else:
            statistics.print_stats()
        # statistics.display_final_stats(side, side, 4)
        # statistics.print_queues_state()
        # statistics.display_received_stats()
    else:
        statistics.print_error()


if __name__ == "__main__":
    main()
