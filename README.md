# SCALPsim: A tool for modeling asynchronous 3-D NoC architectures with heterogeneous links
SCALPsim is a Python simulation framework to study 3-D Network on Chip (NoC) architectures of asynchronous computing elements communicating with heterogeneous links.

The tool has been conceived to be easily changed using class inheritance, particularly for users interested in:
- Routing algorithms.
- Distributed self-organization algorithms.
- High-level topologies or applications.

# Getting started
## Standalone
Requirements:
- Minimum Python version: 3.7
- Python packages: see [requirements.txt](requirements.txt).

## Docker
To use with [Docker](https://docker.com), go to the [docker folder](docker/) and run:
```bash
$ ./run_docker.sh
```

# Reference
A summary of the features and design principles is presented in:

D. Barrientos, C. Sousa, A. Upegui. *"SCALPsim: A tool for modeling asynchronous 3-D NoC architectures with heterogeneous links"*, submitted for publication to [FPL 2020](https://www.fpl2020.org/).
