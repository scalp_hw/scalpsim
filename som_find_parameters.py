#! /usr/bin/python
"""
"""

import click
import subprocess
import sys


@click.command()
@click.option("--inputs")
@click.option("--dimensions")
@click.option("--learn_ratio", type=click.FLOAT, default=1)
@click.option("--side", type=click.INT)
@click.option("--eps_start", type=click.FLOAT)
@click.option("--eps_end", type=click.FLOAT)
@click.option("--sigma_start", type=click.FLOAT)
@click.option("--sigma_end", type=click.FLOAT)
def main(inputs, dimensions, learn_ratio, side, eps_start, eps_end, sigma_start, sigma_end):

    ksomparams = [eps_start, eps_end, sigma_start, sigma_end]
    aqe = None
    lastaqe = None
    iparam = 0
    iplus = 1

    ratio = 0.5
    improved_this_iteration = False
    while ratio >= 2 ** (-16):

        if aqe is not None:
            if lastaqe == None:
                lastaqe = aqe
            elif lastaqe > aqe:
                lastaqe = aqe
                ksomparams = new_ksomparams
                improved_this_iteration = True
            else:
                if iplus:
                    iplus = 0
                else:
                    iplus = 1
                    iparam = (iparam + 1) % 4
                    if iparam == 0:
                        if not improved_this_iteration:
                            ratio /= 2
                        improved_this_iteration = False

            new_ksomparams = ksomparams[:]
            if iplus:
                new_ksomparams[iparam] += ratio * new_ksomparams[iparam]
            else:
                new_ksomparams[iparam] -= ratio * new_ksomparams[iparam]
        else:
            new_ksomparams = ksomparams

        aqe_str = subprocess.check_output(
            [
                "python",
                "som_3dinputs_simulator.py",
                f"--inputs={inputs}",
                f"--learn_ratio={learn_ratio}",
                f"--dimensions={dimensions}",
                f"--side={side}",
                f"--eps_start={new_ksomparams[0]}",
                f"--eps_end={new_ksomparams[1]}",
                f"--sigma_start={new_ksomparams[2]}",
                f"--sigma_end={new_ksomparams[3]}",
                "--final_aqe=True",
            ]
        )

        aqe = float(aqe_str)

        print(
            ", ".join(
                map(
                    str,
                    [
                        aqe,
                        lastaqe,
                        iparam,
                        iplus,
                        ratio,
                        improved_this_iteration,
                        *new_ksomparams,
                    ],
                )
            )
        )
        sys.stdout.flush()


if __name__ == "__main__":
    main()

