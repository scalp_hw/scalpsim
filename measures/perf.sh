#!/bin/bash

# Performance benchmarking script aiming at comparing the different unicast routers.
# Beware, this script with the current parameters takes several hours to execute.

# Test name as argument
testname="$1"
# Starting date
startdate=`date '+%Y%m%d'`

numcycles=2000

for side in 4 6 8
do
    for dist in uniform
    # for dist in uniform sphere_of_locality digitrevert complement
    do
        for router in xyz xyz_adapt adaptive
        # for router in xyz xyz_adapt adaptive virtual_channels
        do
            for links in all_reference eight_routers_3d
            # for links in all_gtp all_lvds all_internal one_router_3d eight_routers_3d
            do
                FILE=${startdate}_${testname}_${router}_${dist}_${side}_${links}.csv
                echo $FILE
                rm -f $FILE
                rate=2
                while [ $rate -le 8192 ]
                do
                    for seed in `seq 1 1 20`
                    do
                        python ../../unicast_gen.py \
                                    --router=$router \
                                    --dist=$dist \
                                    --links=$links \
                                    --side=$side \
                                    --rate=$rate \
                                    --seed=$seed \
                                    --csv=True \
                                    --numcycles=$numcycles \
                                    >> $FILE
                    done
                    rate=$(( $rate * 2 ))
                done
            done
        done
    done
done
