from typing import Type, List, NamedTuple
from matplotlib import pyplot as plt
import pandas as pd
from matplotlib import cm

def get_network_bandwidth(side: int) -> float:
    LINK_BANDWIDTH = 1 / 32
    CHANNELS_PER_LINK = 2
    nodes = (side ** 2) * 4
    # links = 2 * (3 * side ** 3 - 3 * side ** 2)
    bisection = (nodes ** (1 / 3)) ** 2
    bisection_bandwidth = bisection * CHANNELS_PER_LINK * LINK_BANDWIDTH
    prob_cross_bisection = 0.5
    max_network_bandwith = 1 / prob_cross_bisection * bisection_bandwidth
    return max_network_bandwith

def compute_rate(r_in: Type[List], side: int) -> Type[List]:
    nodes = (side ** 2) * 4
    return [1/float(rate) for rate in r_in]


FILENAME = "{}_{}_{}_{}_{}_{}.csv"

routers = {
    "XYZ": "xyz",
    "XYZ adaptive": "xyz_adapt",
    "Weighted adaptive": "adaptive",
}

distributions = {
    "Uniform": "uniform",
}

links_types = {
    "All reference links": "all_reference",
    "2x2x2 distribution": "eight_routers_3d",
}

columns = [
    "Rate",
    "Throughput",
    "Latency",
    "Peek latency",
    "Latency var",
    "Hops",
    "Sent messages",
    "Received messages",
    "Cycles",
    "Full events",
    "Time",
]


class Serie(NamedTuple):
    label: str
    router: str
    distribution: str
    side: int
    links : str

def saveplot(sub_title:str):
    plt.legend()
    plt.tight_layout()
    plt.grid()
    plt.savefig(sub_title.replace(" ", "_").replace(",", ""))
    plt.close()

cm_tab10 = cm.get_cmap('tab10')

def colorAndStyle(iserie:int):
    return {
        'color':cm_tab10(iserie % 3),
        'marker':"o" if iserie<3 else ('P' if iserie<6 else '.'),
        'linestyle':"-" if iserie<3 else (':' if iserie<6 else '--')
     }

def plot_charts(
    series: List[Serie],
    subtitle: str,
    normalize_throughput=True,
    peek_latency=True,
    accepted_workload=True,
) -> None:
    dfs = {}
    date = ""
    test_name = ""
    # average data by rate
    for serie in series:
        try:
            filename = FILENAME.format(date, test_name, serie.router,
                                    serie.distribution, serie.side, serie.links)
            df = pd.read_csv(filename, names=columns)
            if normalize_throughput:
                bandwidth = get_network_bandwidth(serie.side)
                df["Throughput"] /= bandwidth
            df["Rate"] = pd.Series(compute_rate(df["Rate"].tolist(), serie.side))
            dfs[serie] = df
        except FileNotFoundError:
            pass

    # plot latency vs throughput
    plt.figure()
    title = f"Latency vs Applied Load ({subtitle})"
    plt.title(title)
    plt.xlabel("Applied Load (Messages / Cycle per node)")
    plt.ylabel("Average Latency (cycles)")
    for i, serie in enumerate(dfs):
        df = dfs[serie]
        # print(f'latency_{subtitle}', serie)
        # print(df[['Rate', 'Latency']])

        plt.semilogx(
                df.Rate.unique(),
                df.groupby(["Rate"], as_index=False, sort=False).quantile(0.5).Latency,
                label=serie.label,
                **colorAndStyle(i)
            )

        plt.fill_between(df.Rate.unique(),
            df.groupby(["Rate"], as_index=False, sort=False).quantile(0.25).Latency,
            df.groupby(["Rate"], as_index=False, sort=False).quantile(0.75).Latency,
            color=cm_tab10(i % 3),
            alpha=0.14)

        # plt.semilogx(df.Rate,
        #     df_orig.groupby(["Rate"], as_index=False, sort=False).min().Latency,
        #     color=cm_tab10(i % 3),
        #     alpha=0.5)

        # plt.semilogx(df.Rate,
        #     df_orig.groupby(["Rate"], as_index=False, sort=False).max().Latency,
        #     color=cm_tab10(i % 3),
        #     alpha=0.5)

    saveplot(f'img/latency_{subtitle}')
    if peek_latency:
        # plot peek latency vs throughput
        plt.figure()
        title = f"Peak latency vs Applied Load ({subtitle})"
        plt.title(title)
        plt.xlabel("Applied Load (Messages / Cycle per node)")
        plt.ylabel("Peak Latency (cycles)")
        for i, serie in enumerate(dfs):
            df = dfs[serie]
            #  print(f'peek_latency_{subtitle}', serie)
            # print(df[['Rate', 'Peek latency']])

            plt.semilogx(
                df.Rate.unique(),
                df.groupby(["Rate"], as_index=False, sort=False).quantile(0.5)['Peek latency'],
                label=serie.label,
                **colorAndStyle(i)
            )

            plt.fill_between(df.Rate.unique(),
                df.groupby(["Rate"], as_index=False, sort=False).quantile(0.25)['Peek latency'],
                df.groupby(["Rate"], as_index=False, sort=False).quantile(0.75)['Peek latency'],
                color=cm_tab10(i % 3),
                alpha=0.14)


        saveplot(f'img/peek_latency_{subtitle}')

    if accepted_workload:
        # plot latency vs throughput
        plt.figure()
        title = f"Accepted traffic vs Applied Load ({subtitle})"
        plt.title(title)
        plt.xlabel("Applied Load (Messages / Cycle per node)")
        throughput_label = "Normalized accepted traffic" if normalize_throughput else "Accepted traffic (Messages / Cycle per node)"

        plt.ylabel(throughput_label)
        for i, serie in enumerate(dfs):
            df = dfs[serie]
            # print(f'load_{subtitle}', serie)
            # print(df[['Rate', 'Throughput']])
            plt.semilogx(
                df.Rate.unique(),
                df.groupby(["Rate"], as_index=False, sort=False).quantile(0.5).Throughput,
                label=serie.label,
                **colorAndStyle(i)
            )

            plt.fill_between(df.Rate.unique(),
                df.groupby(["Rate"], as_index=False, sort=False).quantile(0.25).Throughput,
                df.groupby(["Rate"], as_index=False, sort=False).quantile(0.75).Throughput,
                color=cm_tab10(i % 3),
                alpha=0.14)

        saveplot(f'img/load_{subtitle}')

for link_title, link_type in links_types.items():
    for side in (4, 6, 8):
        for dist_title, dist in distributions.items():
            series = [
                Serie(label, router, dist, side, link_type) for (label, router) in routers.items()
            ]
            plot_charts(series, f"{dist_title}, {(side**2)*4} nodes, {link_title}")

    for router_name, router in routers.items():
            # if router_name not in ("XYZ", ):
            #     continue
            series = [
                Serie(label, router, dist, 4, link_type)
                for (label, dist) in distributions.items()
            ]
            plot_charts(series, f"{router_name}, {link_title}")

    for dist_title, dist in distributions.items():
            # if router_name != "XYZ":
            #     continue
            series = [Serie(f"{router_name}, {(side**2)*4} nodes", router, dist, side, link_type) for side in (4, 6, 8)
                        for  router_name, router in routers.items()]
            plot_charts(
                series,
                f"{dist_title}, {link_title}",
                normalize_throughput=False,
                peek_latency=True,
                accepted_workload=True,
            )


# plt.show(False)
