from abc import ABC, abstractmethod
from typing import Tuple

from scalp import random, statistics
from scalp.common import Direction, Message, PositionVector
from scalp.communication import PhysicalNode, InChannel
from .__RouteResult import RouteResult


class Router(ABC):
    """
    Algorithm to route incoming messages.
    Different implementations route different types of messages and using diffrent routing algorithms.
    """
    def __init__(self, phys: PhysicalNode, position: PositionVector):
        super().__init__()

        self._phys = phys
        self._position = position
        self._rng = random.create_random_state()


    @abstractmethod
    def perform_route(self) -> RouteResult:
        """
        Try routing a pending message
        :returns: The routing operation result
        """
        pass


    def __str__(self):
        return f"Router {self._position}"
