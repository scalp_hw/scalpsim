from typing import Optional, List
from itertools import takewhile
from random import choice
from scalp import statistics
from scalp.common import Direction, UnicastMessage
from scalp.router import RouteResult
from scalp.communication import InChannel
from .__UnicastRouter import UnicastRouter


class AdaptiveRouter(UnicastRouter):
    """
    Unicast router that routes messages to the soonest available routable direction
    """

    def _route_unicast_msg(self, msg: UnicastMessage) -> RouteResult:
        deltas = [p2 - p1 for p1, p2 in zip(self._position, msg.target)]

        directions = [
            Direction.from_dimension(dim, delta > 0)
            for dim, delta in enumerate(deltas)
            if delta != 0
        ]
        directions_waittimes = sorted(
            [
                (self._phys.wait_time(direction), direction)
                for direction in directions
                if not self._phys.is_full(direction)
            ],
            key=lambda dir_wait: dir_wait[0],
        )
        if not directions_waittimes:
            statistics.interface_full(self._position)
            return RouteResult.FULL_OUTGOING_CHANNELS

        sonnest = directions_waittimes[0][0]
        sonnest_directions = list(
            takewhile(lambda dir_wait: dir_wait[0] == sonnest, directions_waittimes)
        )

        direction = choice(sonnest_directions)[1]
        self._phys.send(direction, msg)
        return RouteResult.MSG_ROUTED
