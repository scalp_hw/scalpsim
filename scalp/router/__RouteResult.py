from typing import Type
from enum import Enum, auto


class RouteResult(Enum):
    """
    The result of a routing operation
    """

    # The values order is important, as we use the MAX operator
    # to combine multiple RouteResults

    # no message found to be routed => wait for new msgs to arrive
    NO_MSG_TO_ROUTE = 1
    # Impossible to route message as outgoing channels are full => retry soon enough
    FULL_OUTGOING_CHANNELS = 2
    # Message has be routed
    MSG_ROUTED = 3

    def merge(self, other: "RouteResult") -> "RouteResult":
        """
        Merges a new RouteResult with the current instance.
        The MAX operator is used to combine multiple route results
        """
        return RouteResult._value2member_map_[max(self.value, other.value)]
