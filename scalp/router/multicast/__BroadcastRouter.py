from abc import abstractmethod
from typing import List, Tuple, Dict
from uuid import UUID
from scalp import random, statistics
from scalp.common import Direction, Message, BroadcastMessage, PositionVector
from scalp.communication import PhysicalNode, InChannel, VcChannel, ReadableChannel
from scalp.router import Router, RouteResult

"""
Broadcast happens in a tree shaped pattern.
Broadcast messages received in a direction are forward in:
 - the same direction as it was received
 - is all dimensions bigger than the one it was received
"""
_BROADCAST_DIRECTIONS_BY_ORIGIN = {
    Direction.FPGA: [  # node is emitter
        Direction.WEST,
        Direction.EAST,
        Direction.NORTH,
        Direction.SOUTH,
        Direction.UP,
        Direction.DOWN,
    ],
    Direction.WEST: [
        Direction.EAST,
        Direction.NORTH,
        Direction.SOUTH,
        Direction.UP,
        Direction.DOWN,
    ],
    Direction.EAST: [
        Direction.WEST,
        Direction.NORTH,
        Direction.SOUTH,
        Direction.UP,
        Direction.DOWN,
    ],
    Direction.NORTH: [Direction.SOUTH, Direction.UP, Direction.DOWN],
    Direction.SOUTH: [Direction.NORTH, Direction.UP, Direction.DOWN],
    Direction.UP: [Direction.DOWN],
    Direction.DOWN: [Direction.UP],
}


class BroadcastRouter(Router):
    """
    Routes a broadcast message so that it reaches each node in the network.
    Network is assumed to be a regular 3D mesh topology.

    The routing algorithm can be descrived as:
        - The node source send the message in all directions (WEST, EAST, NORTH, SOUTH, UP, DOWN)
        - a node that receives a broadcast message sends it to the local computing core but also reroutes copies of the message
          in the direction opposite to the one from where it was received, but also is all directions belonging to higher dimensions.

          Examples of outgoing routing directions per incoming direction:
            Incoming direction -> Outgoing directions
            WEST  -> EAST, NORTH, SOUTH, UP, DOWN
            EAST  -> WEST, NORTH, SOUTH, UP, DOWN
            NORTH -> SOUTH, UP, DOWN
            DOWN  -> UP
            ...
    """
    def __init__(self, phys: PhysicalNode, position: PositionVector):
        super().__init__(phys, position)

        self.__build_broadcast_buffers()

    def __build_broadcast_buffers(self):
        self._broadcast_buffer_by_direction: Dict[Direction, VcChannel] = {}

        physical_directions = [
            Direction.WEST,
            Direction.EAST,
            Direction.NORTH,
            Direction.SOUTH,
            Direction.UP,
            Direction.DOWN,
        ]
        for direction in physical_directions:
            if direction in self._phys.out_channels:
                self._broadcast_buffer_by_direction[direction] = VcChannel(
                    self._phys.clock
                )

    def __sorted_channels(
        self, channels: List[Tuple[Direction, ReadableChannel]]
    ) -> List[Tuple[Direction, ReadableChannel]]:
        """
        Gets all routable channels, sorted by occupation DESC
        """
        routable_channels = [
            (direction, channel)
            for (direction, channel) in channels
            if direction != Direction.ComputingCore and channel.free_space() < 1
        ]
        return sorted(
            routable_channels, key=lambda dir_channel: dir_channel[1].free_space()
        )

    def perform_route(self) -> RouteResult:
        """
        Performs routing operations on broadcast messages
        """

        dirchannels = self.__sorted_channels(self._phys.in_channels.items())
        channels = [channel for direction, channel in dirchannels]
        broadcast_buffers = self.__sorted_channels(
            self._broadcast_buffer_by_direction.items()
        )

        if not channels and not broadcast_buffers:
            # no pending msg
            return RouteResult.NO_MSG_TO_ROUTE

        route_res = self._route_broadcast_buffers(broadcast_buffers)
        if route_res != RouteResult.MSG_ROUTED:
            route_res = route_res.merge(self._route_broadcast_channels(channels))
        return route_res

    def _route_broadcast_buffers(self, broadcast_buffers) -> RouteResult:
        """
        Checks if there are outgoing Broadcast messages in the buffers that that be routed
        """
        if not broadcast_buffers:
            return RouteResult.NO_MSG_TO_ROUTE

        for direction, buffer in broadcast_buffers:
            if not self._phys.is_full(direction):
                msg = buffer.peek()
                self._phys.send(direction, msg)
                buffer.pop()
                return RouteResult.MSG_ROUTED

        return RouteResult.FULL_OUTGOING_CHANNELS

    def _route_broadcast_channels(self, channels: List[InChannel]) -> RouteResult:
        """
        Checks for incomming broadcast messages at the top of a buffer
        """
        if self._phys.is_full(Direction.ComputingCore):
            return RouteResult.FULL_OUTGOING_CHANNELS

        route_res = RouteResult.NO_MSG_TO_ROUTE
        for channel in channels:
            msg = channel.peek()
            if msg and isinstance(msg, BroadcastMessage):
                route_res = route_res.merge(
                    self._route_broadcast_msg(msg, channel.direction)
                )
                if route_res == RouteResult.MSG_ROUTED:
                    statistics.routed(self._position)
                    channel.pop()
                    return RouteResult.MSG_ROUTED

        return route_res

    def _route_broadcast_msg(
        self, msg: BroadcastMessage, origin: Direction
    ) -> RouteResult:
        """
        Try routing multicast message
        """
        boardcast_directions = [
            direction
            for direction in _BROADCAST_DIRECTIONS_BY_ORIGIN[origin]
            if direction in self._phys.out_channels
        ]
        buffers_free = all(
            self._broadcast_buffer_by_direction[direction].free_space() > 0
            for direction in boardcast_directions
        )

        if not buffers_free:
            return RouteResult.FULL_OUTGOING_CHANNELS

        for direction in boardcast_directions:
            msg_copy = msg.duplicate()
            statistics.injected_msg(self._position, self._phys.clock.time, msg_copy)
            self._broadcast_buffer_by_direction[direction].push(msg_copy)

        self._phys.send(Direction.ComputingCore, msg)

        return RouteResult.MSG_ROUTED

    def __str__(self):
        return f"Router {self._position}"
