from abc import abstractmethod
from typing import List, Tuple, Dict, Deque
from uuid import UUID
from collections import deque
from scalp import random, statistics
from scalp.common import (
    Direction,
    Message,
    BroadcastMessage,
    ReduceMessage,
    ReduceResponseMessage,
    PositionVector,
)
from scalp.communication import PhysicalNode, InChannel, VcChannel, ReadableChannel
from scalp.router import Router, RouteResult


_DIRECTION_PER_DIMENSION = [
    [Direction.WEST, Direction.EAST],
    [Direction.SOUTH, Direction.NORTH],
    [Direction.DOWN, Direction.UP],
]


class ReduceRouter(Router):
    """
    Routes ReduceResponseMessage up the broadcast tree, through the inverse path used for the broadcast algorithm.
    For each ReduceResponseMessage, a nodes waits until it has received all respective messages,.
    Expected message include own ReduceResponseMessage plus the ones expected from it's children (as per the broadcast spanning tree used for the broadcast)
    All ReduceResponseMessage are combined into a single one that is send to the node's parent (again, as per the used spanning tree)
    The algorithm complexity is the same as per the broadcast algorithm: linear to the number of nodes in the network.
    """
    def __init__(self, phys: PhysicalNode, position: PositionVector):
        super().__init__(phys, position)

        self.__build_reduce_buffers()

    def __build_reduce_buffers(self):
        self.__reducing_buffer: Dict[
            UUID, List[ReduceResponseMessage, int, Direction]
        ] = {}
        self.__reduced_buffer: Deque[Tuple[ReduceResponseMessage, Direction]] = deque()

    def __sorted_channels(
        self, channels: List[Tuple[Direction, ReadableChannel]]
    ) -> List[Tuple[Direction, ReadableChannel]]:
        """
        Gets all routable channels, sorted by occupation DESC
        """
        routable_channels = [
            (direction, channel)
            for (direction, channel) in channels
            if direction != Direction.ComputingCore and channel.free_space() < 1
        ]
        return sorted(
            routable_channels, key=lambda dir_channel: dir_channel[1].free_space()
        )

    def perform_route(self) -> RouteResult:
        """
        Performs routing operations on reduce messages and responses
        """

        dirchannels = self.__sorted_channels(self._phys.in_channels.items())
        channels = [channel for direction, channel in dirchannels]

        if not channels and not self.__reduced_buffer:
            # no pending msg
            return RouteResult.NO_MSG_TO_ROUTE

        self._combine_reduce(channels)

        return self._route_reduced()

    def _combine_reduce(self, channels: List[InChannel]):
        """
        Searches for ReduceResponseMessages at in the incomming channel
        We always return false as we are not considering the combine operation as a routing operation
        """
        for channel in channels:
            msg = channel.peek()
            while msg and isinstance(msg, ReduceResponseMessage):
                self._handle_reduceresponse_msg(msg)
                channel.pop()
                msg = channel.peek()

    def _get_reduce_msg_info(self, msg: ReduceResponseMessage) -> Tuple[int, Direction]:
        """
        Calculates the number of reduce messages expected for the reduce operation
        associated with the msg in the parameter and the forwarding direction when all msgs have been received.
        The pattern for the reduction follows the same tree as in the broadcast (see BroadcastRouter)
        although in the opposite path direction:

        ie: same dimension => expected directions
            (x, y, z) => W, E, N, S, U, D
            (y, z)    => N, S, U, D + (E if node.x > target.x else W )
            (z,)      => U, D + (N if node.y > target.y else S )
            (,)       => U if node.z > target.z else D
        """

        forward_direction = Direction.ComputingCore
        expected_directions = [Direction.ComputingCore]
        for dimension in reversed(range(3)):
            dimension_directions = _DIRECTION_PER_DIMENSION[dimension]
            if msg.target[dimension] == self._position[dimension]:
                expected_directions += dimension_directions
            else:
                if msg.target[dimension] > self._position[dimension]:
                    incoming_direction, forward_direction = dimension_directions
                else:
                    forward_direction, incoming_direction = dimension_directions
                expected_directions += [incoming_direction]
                break

        return (
            len(
                [
                    direction
                    for direction in expected_directions
                    if direction in self._phys.out_channels
                ]
            ),
            forward_direction,
        )

    def _handle_reduceresponse_msg(self, msg: ReduceResponseMessage):
        if msg.uuid not in self.__reducing_buffer:
            reduce_count, forward_direction = self._get_reduce_msg_info(msg)
            self.__reducing_buffer[msg.uuid] = [
                msg,
                reduce_count - 1,
                forward_direction,
            ]
        else:
            self.__reducing_buffer[msg.uuid][0].combine(msg)
            self.__reducing_buffer[msg.uuid][1] -= 1

        if self.__reducing_buffer[msg.uuid][1] == 0:
            """
            Received all expected reduce msgs for the associated uuid
            """
            msg, _, direction = self.__reducing_buffer[msg.uuid]
            del self.__reducing_buffer[msg.uuid]
            self.__reduced_buffer.appendleft((msg, direction))
        else:
            statistics.received(self._position, self._phys.clock.time, msg)

    def _route_reduced(self) -> RouteResult:
        if not self.__reduced_buffer:
            return RouteResult.NO_MSG_TO_ROUTE

        msg, direction = self.__reduced_buffer[-1]
        if self._phys.is_full(direction):
            return RouteResult.FULL_OUTGOING_CHANNELS

        self._phys.send(direction, msg)
        self.__reduced_buffer.pop()
        return RouteResult.MSG_ROUTED

    def __str__(self):
        return f"Router {self._position}"
