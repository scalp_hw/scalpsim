from enum import Enum
from typing import Tuple, Dict, List, Optional
from threading import Event
from scalp.common import Direction, Message, PositionVector
from .__InChannel import InChannel
from .__OutChannel import OutChannel
from scalp.perf.Clock import Clock


class PhysicalNode:
    """
    Layer that groups all the incomming and outgoing channels per NoC
    """
    def __init__(self, pos: PositionVector, clock: Clock):
        self.__clock = clock
        self.__in_event = Event()
        self.__out_channels: Dict[Direction, OutChannel] = {}
        self.__in_channels: Dict[Direction, InChannel] = {
            direction: InChannel(self.__in_event, pos, direction, clock)
            for direction in Direction
        }

        for direction in (Direction.FPGA, Direction.ComputingCore):
            self.__out_channels[direction] = OutChannel(self.__in_channels[direction])

    @property
    def clock(self) -> Clock:
        return self.__clock

    @property
    def in_channels(self) -> Dict[Direction, InChannel]:
        return self.__in_channels

    @property
    def out_channels(self) -> Dict[Direction, OutChannel]:
        return self.__out_channels

    def free_space(self, direction: Direction) -> float:
        assert (
            direction in self.__out_channels
        ), f"{direction} if not an available outgoing direction"
        return self.__out_channels[direction].free_space()

    def wait_time(self, direction: Direction) -> float:
        assert (
            direction in self.__out_channels
        ), f"{direction} if not an available outgoing direction"
        return self.__out_channels[direction].wait_time()

    def is_empty(self, direction: Direction) -> bool:
        return self.free_space(direction) == 1

    def is_full(self, direction: Direction) -> bool:
        return self.free_space(direction) == 0

    def send(self, direction: Direction, msg: Message) -> None:
        assert (
            direction in self.__out_channels
        ), "Sending to inexistent outgoing channel"
        self.__out_channels[direction].send(msg)

    def wait_msg(self, timeout: float = None) -> Dict[Direction, InChannel]:
        if self.__in_event.wait(timeout=timeout):
            # Clear event only when wait exits after event set, not timeout
            self.__in_event.clear()
        return self.__in_channels

    def attach_neighbour_channel(self, direction: Direction, channel: InChannel):
        self.__out_channels[direction] = OutChannel(channel)
