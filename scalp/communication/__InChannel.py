from typing import Deque, Optional, ClassVar
from threading import Event
from collections import deque

"""
Deques support thread-safe, memory efficient appends and pops from either side of the
deque with approximately the same O(1) performance in either direction.
 - https://docs.python.org/3/library/collections.html#deque-objects
"""

from scalp.common import Message, Direction, PositionVector
from scalp import statistics, perf
from scalp.perf.Clock import Clock
from .__ReadableChannel import ReadableChannel


class InChannel(ReadableChannel):
    """
    Incoming channel, receiving messages from neighbours
    """
    FIFO_SIZE: ClassVar[int] = 32

    def __init__(
        self,
        receiver_event: Event,
        pos: PositionVector,
        direction: Direction,
        clock: Clock,
    ) -> None:
        self.__queue: Deque[Message] = deque()
        self.__receiver_event = receiver_event
        self.__nif: perf.NIFConfig = perf.get_interface(direction, pos)
        self.__pos = pos
        self.__direction = direction
        self.__channel_free_time: int = 0
        self.__clock = clock

    @property
    def direction(self):
        return self.__direction

    def pop(self) -> Message:
        msg = self.__queue.pop()
        self.__update_msg_delay(msg)
        return msg

    def __update_msg_delay(self, msg: Message) -> None:
        delta_time = self.__clock.time - msg.stats.last_update_time
        msg.stats.transport_time += delta_time
        msg.stats.current_hop_time += delta_time
        msg.stats.max_hop_time = max(msg.stats.max_hop_time, msg.stats.current_hop_time)
        msg.stats.last_update_time = self.__clock.time

    def peek(self) -> Optional[Message]:
        if not self.__queue:
            return None
        msg = self.__queue[-1]
        self.__update_msg_delay(msg)
        return msg

    def size(self) -> int:
        return len(self.__queue)

    def push(self, msg: Message):
        assert (
            len(self.__queue) < InChannel.FIFO_SIZE
        ), "Cannot push new message. Incoming queue is full"

        # NOC hops are not counted in the performance metrics due to their
        # unique performance caracteristics
        msg.stats.current_hop_time = 0
        if self.__direction != Direction.FPGA and self.__direction != Direction.ComputingCore:
            msg.stats.hops += 1
            # the message can be sent once the wire is free (self.__channel_free_time)
            time_until_channel_free = self.wait_time()
            send_duration = int(1 / self.__nif.throughput)
            time_until_channel_free_again = time_until_channel_free + send_duration
            self.__channel_free_time = time_until_channel_free_again + self.__clock.time
            msg.stats.current_hop_time = (
                time_until_channel_free_again + self.__nif.latency
            )

        msg.stats.transport_time += msg.stats.current_hop_time
        msg.stats.last_update_time = self.__clock.time
        self.__queue.appendleft(msg)

        if self.__direction != Direction.ComputingCore:
            # do not wake up routing for ComputingCore addressed messages
            self.__receiver_event.set()

    def wait_time(self) -> int:
        """The time to weait until the channel is available
        """
        return max(0, self.__channel_free_time - self.__clock.time)


    def free_space(self) -> float:
        """Free space left, in [0, 1]. 0->full, 1->empty
        """
        return 1 - self.size() / InChannel.FIFO_SIZE

    def __str__(self) -> str:
        # return f"{self.__direction}@{self.__pos}: {self.__nif}"
        return "{0:30s} {1:35s} {2:s}".format(str(self.__direction), str(self.__pos), str(self.__nif))
