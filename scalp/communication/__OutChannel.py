from typing import Deque, Optional
from threading import Event
from collections import deque

from scalp.common import Message, Direction, PositionVector
from scalp import statistics, perf
from scalp.perf.Clock import Clock
from .__InChannel import InChannel

class OutChannel:
    """
    Outgoing channel, linked to a neighboring incoming channel
    """
    def __init__(self, in_channel: InChannel) -> None:
        self.__in_channel = in_channel

    def send(self, msg: Message) -> None:
        self.__in_channel.push(msg)

    def free_space(self) -> float:
        return self.__in_channel.free_space()

    def wait_time(self) -> int:
        return self.__in_channel.wait_time()

    def __str__(self) -> str:
        return str(self.__in_channel)
