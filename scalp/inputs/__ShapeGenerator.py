import numpy as np
from typing import List, Tuple, Iterator as TypeIterator
from collections.abc import Iterator
from scalp.random import create_random_state

class ShapeGenerator(Iterator):
    """
    Generates multi-dimensional random random, with different boundaries per dimension
    """
    def __init__(self, bounds:List[Tuple[int, int]]=[(0, 1), (0, 1)]):
        self.bounds = bounds
        self.dimensions = len(bounds)
        self.random_state = create_random_state(999)

    def __next__(self) -> TypeIterator:
        val = self.random_state.rand(1, self.dimensions)[0]
        for i, bound in enumerate(self.bounds):
            val[i] *= abs(bound[1] - bound[0])
            val[i] += bound[0]
        return val
