import numpy as np
from typing import Iterator as TypeIterator
from collections.abc import Iterator

class Combine(Iterator):
    """
    Combines multiple generators.
    Generators are chosen to generate inputs, either equiprobably or weighted as per a specified weigth ('p')
    """
    def __init__(self, generators, p=None):
        """
        p : 1-D array-like, optional
        The probabilities associated with each entry in generators. If not given the sample assumes a uniform distribution over all entries in generators.
        """

        assert generators is not None
        assert len(generators) >= 1

        self.generators = generators
        self.p = p

    def __next__(self) -> TypeIterator:
        gen_idx = np.random.choice(len(self.generators), p=self.p)
        gen = self.generators[gen_idx]
        return next(gen)
