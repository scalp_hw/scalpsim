
import numpy as np
from scalp.random import create_random_state
from typing import Iterator as TypeIterator
from collections.abc import Iterator

class RandomGenerator(Iterator):
    """
    Generates multid-imensional random numbers between specified boundaries.
    """
    def __init__(self, low: int, high: int, dimensions=1):
        self.low = low
        self.high = high
        self.dimensions = dimensions
        self.random_state = create_random_state(999)


    def __next__(self) -> TypeIterator:
        return self.random_state.uniform(low=self.low, high=self.high, size=self.dimensions)
