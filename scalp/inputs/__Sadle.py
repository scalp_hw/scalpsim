from scalp.random import create_random_state
from collections.abc import Iterator

class SadleGenerator(Iterator):
    """
    Generates multi-dimensional random random, with different boundaries per dimension
    """
    def __init__(self):
        self.random_state = create_random_state(999)

    def __next__(self):
        x, y = self.random_state.uniform(low=-1, high=1.0, size=2)
        return (x, y, x**2 - y**2)
