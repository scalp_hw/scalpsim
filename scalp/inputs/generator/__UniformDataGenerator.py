from threading import Thread, Lock
import numpy as np
from scalp import random
from time import sleep
from scalp import Network
from scalp import statistics
from scalp.common import Message, PositionVector, YIELD_EPSILON, Direction


class UniformDataGenerator:
    """
    Deprecated
    """
    BACKOFF_DELAY = 0.02
    MAX_THREAD_TAKE_COUNT = 100

    def __init__(self, communication: Network)->None:
        self.__network = communication
        self.__rng = random.create_random_state()
        self.__lock = Lock()

    def generate(self, msg_count: int, threads_count:int)->None:
        assert 1 <= threads_count <= msg_count, 'threads_count must in [1, msg_count]'
        assert msg_count > 0, 'msg_count must be bigger than "1"'
        self.__msg_count = msg_count
        self.__thread_take_count = min(self.MAX_THREAD_TAKE_COUNT, int(msg_count/threads_count))

        for i in range(threads_count):
            thread = Thread(target=self.__generate_data,name=f'UniformDataGenerator {i}', args=(i, ))
            thread.daemon = True
            thread.start()

    def __thread_take(self):
        with self.__lock:
            delta = min(self.__msg_count, self.__thread_take_count)
            self.__msg_count -= delta
        return delta

    def __pick_random_position(self, different_from: PositionVector=None):
        rnd_pos = different_from
        while rnd_pos == different_from:
            rnd_pos = PositionVector(*[self.__rng.randint(0, dim_size)
                                for dim_size in self.__network.size])
        return rnd_pos

    def __generate_data(self, ithread) -> None:
        statistics.started()

        try:
            msg_count = self.__thread_take()
            while msg_count > 0:
                for i in range(msg_count):
                    from_pos = self.__pick_random_position()
                    to_pos = self.__pick_random_position(from_pos)
                    from_node = self.__network.nodes[from_pos]
                    msg = UnicastMessage(from_pos=from_pos, to_pos=to_pos, payload=f'{i}@{ithread}')

                    while from_node.physicalnode.is_full(Direction.FPGA):
                        sleep(YIELD_EPSILON+np.random.random_sample() * self.BACKOFF_DELAY)

                    from_node.physicalnode.send(Direction.FPGA, msg)

                msg_count = self.__thread_take()
        except:
            statistics.error()
            raise
        statistics.finished()
