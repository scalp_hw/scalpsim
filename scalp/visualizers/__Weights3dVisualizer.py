from typing import Dict, Tuple, Optional, List
from scalp import Network
from scalp.common import PositionVector
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.animation import ImageMagickWriter


class Weights3dVisualizer:
    """
    Animated plots for 2D vectors.
    Display the node's weigths and the inputs vectors
    """

    def __init__(self, network):
        self.__network = network
        figure = plt.figure(figsize=(8, 4))
        self.__ax = figure.add_subplot(1, 1, 1, projection="3d")

    def draw_input_vectors(self, input_vector: List[PositionVector]) -> None:
        if input_vector:
            vectors = list(zip(*input_vector))
            self.__ax.scatter(
                xs=vectors[0],
                ys=vectors[1],
                zs=vectors[2],
                c='#666666',
                marker='o',
                s=8
            )


    def plot_network(self):
        plt_links = {}

        for node_pos in self.__network.nodes:
            # plot synapsis
            neighbour_positions = self.__network.get_neighbours(node_pos)
            node = self.__network.nodes[node_pos]
            for neighbour_position in neighbour_positions:
                if (neighbour_position, node_pos) in plt_links:
                    continue
                neighbour = self.__network.nodes[neighbour_position]
                plt_obj = self.__ax.plot(
                    *zip(node.computingcore.weights, neighbour.computingcore.weights),
                    "-b",
                    # alpha=0.3,
                )
                plt_links[(node_pos, neighbour_position)] = plt_obj[0]

        # Plot neurons getting their positions from weights
        weights = list(zip(*[n.computingcore.weights for n in self.__network.nodes.values()]))
        self.__ax.scatter(
            xs=weights[0],
            ys=weights[1],
            zs=weights[2],
            c='r',
            marker='o',
            s=20
        )
        # Title
        #self.__ax.set_title("Weights 3D")
        #self.__ax.set_xlabel("X")
        #self.__ax.set_ylabel("Y")
        #self.__ax.set_zlabel("Z")

        self.__ax.xaxis.pane.fill = False
        self.__ax.yaxis.pane.fill = False
        self.__ax.zaxis.pane.fill = False

        # Now set color to white (or whatever is "invisible")
        self.__ax.xaxis.pane.set_edgecolor('w')
        self.__ax.yaxis.pane.set_edgecolor('w')
        self.__ax.zaxis.pane.set_edgecolor('w')

        plt.axis('off')


        # Bonus: To get rid of the grid as well:
        self.__ax.grid(False)

        plt.tight_layout()

        # Show figure
        self.__ax.view_init(15, -45)
        plt.show(block=True)
