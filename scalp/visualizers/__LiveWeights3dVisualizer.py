from scalp.common import PositionVector
import matplotlib.pyplot as plt
from matplotlib.animation import ImageMagickWriter


class LiveWeights3dVisualizer:
    """
    Animated plots for 2D vectors.
    Display the node's weigths and the inputs vectors
    """

    def __init__(self, title, network, inputs,live=True, save=None):
        self.__network = network
        self.__live = live
        self.__writer = None
        self.__inputs = inputs
        self.__title = title
        self.__figure, self.__ax, self.__plt_dots, self.__plt_links = self.__plot(title)

        if save:
            self.__writer = ImageMagickWriter(fps=30)
            self.__writer.setup(fig=self.__figure, outfile=save, dpi=100)

    def update(self, input_vector=None, input_nb=0) -> None:
        """
        Plots the new input_vector and updates the nodes weights
        """
        if not self.__live:
            return

        if input_vector:
            self.__update_figure()
            self.__draw_input_vector(input_vector)
            plt.suptitle(f"{self.__title}\n{input_nb}/{len(self.__inputs)}")

        if self.__writer:
            self.__writer.grab_frame()

        self.__figure.canvas.draw()
        self.__figure.canvas.flush_events()
        plt.pause(0.001)

    def __draw_input_vector(self, input_vector: PositionVector) -> None:
        self.__ax.scatter(
            xs=input_vector[0],
            ys=input_vector[1],
            zs=input_vector[2],
            c='#666666',
            marker='o',
            alpha=0.9,
            s=8
        )


    def finish(self):
        if not self.__live:
            self.__update_figure()
            vectors = list(zip(*self.__inputs))
            self.__ax.scatter(
                xs=vectors[0],
                ys=vectors[1],
                zs=vectors[2],
                c='#666666',
                marker='o',
                s=8
            )

        if self.__writer:
            self.__writer.finish()

        plt.show()


    def __update_figure(self) -> None:

        for (node1, node2), line in self.__plt_links.items():

            weigths = [
                self.__network.nodes[node_pos].computingcore.weights
                for node_pos in (node1, node2)
            ]
            line.set_data_3d(*zip(*weigths))

        self.__plt_dots._offsets3d = list(zip(self.__network.nodes.values()))


        pos_x = [n.computingcore.weights[0] for n in self.__network.nodes.values()]
        pos_y = [n.computingcore.weights[1] for n in self.__network.nodes.values()]
        pos_z = [n.computingcore.weights[2] for n in self.__network.nodes.values()]
        self.__plt_dots._offsets3d = (pos_x, pos_y, pos_z)


    def __plot(self, title="Weights 3d"):
        figure = plt.figure()
        ax = figure.add_subplot(1, 1, 1, projection="3d")

        plt_links = {}

        for node_pos in self.__network.nodes:
            # plot synapsis
            neighbour_positions = self.__network.get_neighbours(node_pos)
            node = self.__network.nodes[node_pos]
            for neighbour_position in neighbour_positions:
                if (neighbour_position, node_pos) in plt_links:
                    continue
                neighbour = self.__network.nodes[neighbour_position]
                plt_obj = ax.plot(
                    *zip(node.computingcore.weights, neighbour.computingcore.weights),
                    "-b",
                    alpha=0.6
                    # alpha=0.3,
                )
                plt_links[(node_pos, neighbour_position)] = plt_obj[0]

        # Plot neurons getting their positions from weights
        weights = list(zip(*[n.computingcore.weights for n in self.__network.nodes.values()]))
        plt_dots = ax.scatter(
            xs=weights[0],
            ys=weights[1],
            zs=weights[2],
            c='r',
            marker='o',
            alpha=0.5,
            s=5
        )

        ax.set_xlim(-1, 1)
        ax.set_ylim(-.7, 1)
        ax.set_zlim(-.7, 1)

        ax.xaxis.pane.fill = False
        ax.yaxis.pane.fill = False
        ax.zaxis.pane.fill = False

        # Now set color to white (or whatever is "invisible")
        ax.xaxis.pane.set_edgecolor('w')
        ax.yaxis.pane.set_edgecolor('w')
        ax.zaxis.pane.set_edgecolor('w')

        plt.axis('off')
        ax.grid(False)

        ax.view_init(15, -45)

        plt.suptitle(f"{title}\n")
        plt.tight_layout()
        if self.__live:
            plt.show(block=False)

        return figure, ax, plt_dots, plt_links

