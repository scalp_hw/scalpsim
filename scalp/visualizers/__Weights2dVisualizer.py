from typing import Dict, Tuple, Optional
from scalp import Network
from scalp.common import PositionVector
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.animation import ImageMagickWriter


class Weights2dVisualizer:
    """
    Animated plots for 2D vectors.
    Display the node's weigths and the inputs vectors
    """
    def __init__(self, network, input_count:int, live=True, save=Optional[str]):
        self.__network = network
        self.__live = live
        self.__writer = None
        self.__input_count = input_count
        self.__figure, self.__plt_dots, self.__plt_links = self.__plot()
        if save:
            self.__writer = ImageMagickWriter(fps=15)
            self.__writer.setup(fig=self.__figure, outfile=save, dpi=100)

    def update(self, input_vector: PositionVector, input_nb:int) -> None:
        """
        Plots the new input_vector and updates the nodes weights
        """
        self.__update_figure()
        self.__draw_input_vector(input_vector)
        plt.suptitle(f'Inputs: {input_nb}/{self.__input_count}')
        if self.__writer:
            self.__writer.grab_frame()

        self.__figure.canvas.draw()
        self.__figure.canvas.flush_events()


    def finish(self):
        if self.__writer:
            self.__writer.finish()

    def __draw_input_vector(self, input_vector: PositionVector) -> None:
        plt.plot(input_vector[0], input_vector[1], ".g")

    def __update_figure(self) -> None:

        for (node1, node2), line in self.__plt_links.items():

            weigths = [
                self.__network.nodes[node_pos].computingcore.weights
                for node_pos in (node1, node2)
            ]
            line.set_data(*zip(*weigths))

        pos_x = [n.computingcore.weights[0] for n in self.__network.nodes.values()]
        pos_y = [n.computingcore.weights[1] for n in self.__network.nodes.values()]
        self.__plt_dots.set_data(pos_x, pos_y)

    def __plot(self, title="Weights 2d"):
        figure = plt.figure()
        plt_links = {}

        for node_pos in self.__network.nodes:
            # plot synapsis
            neighbour_positions = self.__network.get_neighbours(node_pos)
            node = self.__network.nodes[node_pos]
            for neighbour_position in neighbour_positions:
                if (neighbour_position, node_pos) in plt_links:
                    continue
                neighbour = self.__network.nodes[neighbour_position]
                plt_obj = plt.plot(*zip(node.computingcore.weights, neighbour.computingcore.weights), "-b")
                plt_links[(node_pos, neighbour_position)] = plt_obj[0]

        # Plot neurons getting their positions from weights
        pos_x = [n.computingcore.weights[0] for n in self.__network.nodes.values()]
        pos_y = [n.computingcore.weights[1] for n in self.__network.nodes.values()]
        plt_dots = plt.plot(pos_x, pos_y, "or")[0]

        plt.tight_layout()

        # Title
        plt.title(title)
        # Show figure
        if self.__live:
            plt.show(block=False)
        return figure, plt_dots, plt_links
