"""
Module enabling the configuration of the network interfaces as per the physical SCALP links.
Interfaces are characterized by throughput (in messages / cycle) and latency (in cycles).
"""
from collections import defaultdict
from .__NIFConfig import NIFConfig
from scalp.common import Direction, PositionVector


class __PerfConfig:
    def __init__(self):
        self.route_cost: int = 0
        self.interface_config: Dict[PositionVector, Dict[Direction, NIFConfig]] = defaultdict(dict)
        self.size_router_types: PositionVector = (1, 1, 1)


__perfConfig = __PerfConfig()


def set_route_cost(route_cost: int) -> None:
    """
    Defines the cost, in cycles, for 1 routing operation
    """
    __perfConfig.route_cost = route_cost


def route_cost() -> int:
    return __perfConfig.route_cost

def set_size_router_types(size: PositionVector) -> None:
    """
    Set number of different router types in each dimension
    """
    __perfConfig.size_router_types = size

def set_interface(direction: Direction, config: NIFConfig, local_pos: PositionVector = (0, 0, 0)) -> None:
    """
    Set link configuration of all nodes of a certain type according to their position
    """
    __perfConfig.interface_config[local_pos][direction] = config


def get_interface(direction: Direction, global_pos: PositionVector) -> NIFConfig:
    """
    Get the link configuration of a node according to its position in the network
    """
    local_pos = PositionVector(*[x % y for x, y in zip(global_pos, __perfConfig.size_router_types)])
    assert (
        local_pos in __perfConfig.interface_config
    ), f"Position {local_pos} extracted from {global_pos} with a router type of {__perfConfig.size_router_types} not found"
    assert (
        direction in __perfConfig.interface_config[local_pos]
    ), f"Direction {direction} has not been configured for {local_pos}"
    return __perfConfig.interface_config[local_pos][direction]
