"""Link configurations with multiple routers per physical SCALP node."""

import itertools
from scalp import perf
from scalp.perf.links import GTPLink, LVDSLink, InternalLink
from scalp.common import Direction

def one_router_3d() -> None:
    """
    Use one router per SCALP node.
    """
    perf.set_route_cost(5)  # cost in cycles for 1 routing operation
    perf.set_interface(Direction.EAST, GTPLink)
    perf.set_interface(Direction.WEST, GTPLink)
    perf.set_interface(Direction.NORTH, GTPLink)
    perf.set_interface(Direction.SOUTH, GTPLink)
    perf.set_interface(Direction.UP, LVDSLink)
    perf.set_interface(Direction.DOWN, LVDSLink)
    perf.set_interface(Direction.ComputingCore, InternalLink)
    perf.set_interface(Direction.FPGA, InternalLink)

def eight_routers_3d() -> None:
    """
    Use a 2x2x2 network with all link types.
    """
    perf.set_route_cost(5)  # cost in cycles for 1 routing operation
    perf.set_size_router_types((2, 2, 2))
    for i, j, k in itertools.product(range(2), range(2), range(2)):
        if i == 0:
            perf.set_interface(Direction.EAST, InternalLink, (i, j, k))
            if j == 0 and k == 0:
                perf.set_interface(Direction.WEST, GTPLink, (i, j, k))
            else:
                perf.set_interface(Direction.WEST, LVDSLink, (i, j, k))
        else:
            perf.set_interface(Direction.WEST, InternalLink, (i, j, k))
            if j == 0 and k == 0:
                perf.set_interface(Direction.EAST, GTPLink, (i, j, k))
            else:
                perf.set_interface(Direction.EAST, LVDSLink, (i, j, k))
        if j == 0:
            perf.set_interface(Direction.NORTH, InternalLink, (i, j, k))
            if i == 0 and k == 1:
                perf.set_interface(Direction.SOUTH, GTPLink, (i, j, k))
            else:
                perf.set_interface(Direction.SOUTH, LVDSLink, (i, j, k))
        else:
            perf.set_interface(Direction.SOUTH, InternalLink, (i, j, k))
            if i == 0 and k == 1:
                perf.set_interface(Direction.NORTH, GTPLink, (i, j, k))
            else:
                perf.set_interface(Direction.NORTH, LVDSLink, (i, j, k))
        if k == 0:
            perf.set_interface(Direction.UP, InternalLink, (i, j, k))
            perf.set_interface(Direction.DOWN, LVDSLink, (i, j, k))
        else:
            perf.set_interface(Direction.UP, LVDSLink, (i, j, k))
            perf.set_interface(Direction.DOWN, InternalLink, (i, j, k))

        perf.set_interface(Direction.ComputingCore, InternalLink, (i, j, k))
        perf.set_interface(Direction.FPGA, InternalLink, (i, j, k))

def two_routers_2d() -> None:
    """
    Use a 2x2 network with GTP and LVDS links towards other boards.
    """
    perf.set_route_cost(5)  # cost in cycles for 1 routing operation
    perf.set_size_router_types((2, 2, 1))
    for i, j, k in itertools.product(range(2), range(2), range(1)):
        if i == 0:
            perf.set_interface(Direction.EAST, InternalLink, (i, j, k))
            if j == 0:
                perf.set_interface(Direction.WEST, GTPLink, (i, j, k))
            else:
                perf.set_interface(Direction.WEST, LVDSLink, (i, j, k))
        else:
            perf.set_interface(Direction.WEST, InternalLink, (i, j, k))
            if j == 0:
                perf.set_interface(Direction.EAST, GTPLink, (i, j, k))
            else:
                perf.set_interface(Direction.EAST, LVDSLink, (i, j, k))
        if j == 0:
            perf.set_interface(Direction.NORTH, InternalLink, (i, j, k))
            if i == 0:
                perf.set_interface(Direction.SOUTH, GTPLink, (i, j, k))
            else:
                perf.set_interface(Direction.SOUTH, LVDSLink, (i, j, k))
        else:
            perf.set_interface(Direction.SOUTH, InternalLink, (i, j, k))
            if i == 0:
                perf.set_interface(Direction.NORTH, GTPLink, (i, j, k))
            else:
                perf.set_interface(Direction.NORTH, LVDSLink, (i, j, k))
        perf.set_interface(Direction.UP, InternalLink, (i, j, k))
        perf.set_interface(Direction.DOWN, InternalLink, (i, j, k))
        perf.set_interface(Direction.ComputingCore, InternalLink, (i, j, k))
        perf.set_interface(Direction.FPGA, InternalLink, (i, j, k))
