"""Link configurations with all equal link types."""

from scalp import perf
from scalp.perf.links import GTPLink, LVDSLink, InternalLink, ReferenceLink
from scalp.common import Direction


def __all_equal(link_type: perf.NIFConfig) -> None:
    """
    Set links for 3-D grid all to the same type.
    """
    perf.set_route_cost(5)  # cost in cycles for 1 routing operation
    for direct in (Direction.EAST, Direction.WEST, Direction.NORTH,
                   Direction.SOUTH, Direction.UP, Direction.DOWN):
        perf.set_interface(direct, link_type)
    # ComputingCore and FPGA set to internal
    perf.set_interface(Direction.ComputingCore, InternalLink)
    perf.set_interface(Direction.FPGA, InternalLink)

def all_gtp() -> None:
    __all_equal(GTPLink)

def all_lvds() -> None:
    __all_equal(LVDSLink)

def all_internal() -> None:
    __all_equal(InternalLink)

def all_reference() -> None:
    __all_equal(ReferenceLink)
