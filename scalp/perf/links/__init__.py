from .__LinkTypes import GTPLink, LVDSLink, InternalLink, ReferenceLink, MSG_SIZE
from .__AllEqual import all_gtp, all_lvds, all_internal, all_reference
from .__MultipleTypes import one_router_3d, eight_routers_3d, two_routers_2d
