"""Basic common link configurations."""

from scalp.perf import NIFConfig

## Message size, in bytes
MSG_SIZE = 32

GTPLink = NIFConfig(throughput=1 / MSG_SIZE, latency=50)
LVDSLink = NIFConfig(throughput=1 / (2*MSG_SIZE), latency=10)
InternalLink = NIFConfig(throughput=1, latency=0)
ReferenceLink = NIFConfig(throughput=1 / MSG_SIZE, latency=10)
