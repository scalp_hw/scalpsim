from typing import Dict


class NIFConfig:
    """
    Network interface performance properties
    """

    def __init__(self, throughput: float, latency: int) -> None:
        """
        :param: throughput Defines throughput as fraction of message send per cycle
        :param: latency Defines latency in cycles
        """

        self.throughput = throughput
        self.latency = latency

    def __str__(self) -> str:
        # return f"Latency: {self.latency} cycles   Throughput: {self.throughput} msg/cycle"
        return "Latency: {0:3d} cycles   Throughput: {1:.4f} msg/cycle".format(self.latency, self.throughput)
