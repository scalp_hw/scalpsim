"""
Helper module that generates numpy RandomState based on a global seed + local seed
"""
from typing import Optional
import numpy as np

__seed: Optional[int] = None


def seed(val: int) -> None:
    global __seed
    __seed = val


def create_random_state(seed: int = 0) -> np.random.RandomState:
    combined_seed = None if __seed is None else __seed + seed
    return np.random.RandomState(combined_seed)
