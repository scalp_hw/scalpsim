
from threading import Thread, Event
from typing import List, ClassVar, Type
import numpy as np
import logging
import progressbar
from time import sleep
from scalp import statistics
from scalp.common import PositionVector


class CustomScheduler:
    """
    Custom event scheduler.
    """

    # Variables to compute probabilites
    K1_FROM: ClassVar[float] = 0.90
    K1_TO: ClassVar[float] = 1.0
    K2_FROM: ClassVar[float] = -0.05
    K2_TO: ClassVar[float] = 0.05
    WAIT_ALL : ClassVar[int] = 0      # Number of cycles: 0 means no wait

    def __init__(
        self, start_event: Event, end_event: Event, wait_events: List[Event],
        ack_events: List[Event], rng: np.random.RandomState, num_iter: int = None
    ) -> None:
        self.__start_event = start_event
        self.__end_event = end_event
        self.__wait_events = wait_events
        self.__ack_events = ack_events
        self.__rng = rng
        self.__num_iter = num_iter

        self.__thread = Thread(target=self.__run, name=f"Scheduler")
        self.__thread.daemon = True
        self.__thread.start()


    def __get_random(self, size: int,
                           from_value: float,
                           to_value: float
                    ) -> Type[np.array]:
        """
        Get random value between two limits using rng.
        """
        return np.add((self.__rng.rand(size)*(to_value - from_value)), from_value)

    def __run(self):
        """
        Wait for start_event, generate wait_events and wait for all ack_events.
        """
        # Initialize iteration variables
        n_wait_events = len(self.__wait_events)
        last_iteration = np.zeros(n_wait_events)
        k1_values = self.__get_random(n_wait_events,
                                      CustomScheduler.K1_FROM,
                                      CustomScheduler.K1_TO)
        probabilities = np.zeros(n_wait_events)
        iteration_counter = 0
        if self.__num_iter is None:
            pbar = progressbar.ProgressBar(max_value=progressbar.UnknownLength)
        else:
            pbar = progressbar.ProgressBar(max_value=self.__num_iter)

        self.__start_event.wait()
        statistics.started()
        logging.debug("Starting scheduler")

        def loop_check(it) -> bool:
            pbar.update(it)
            if self.__num_iter is None:
                return not self.__end_event.is_set()
            else:
                return bool(it < self.__num_iter)

        while loop_check(iteration_counter):
            iteration_counter += 1
            # Compute probabilities
            k2_values = self.__get_random(n_wait_events,
                                          CustomScheduler.K2_FROM*n_wait_events,
                                          CustomScheduler.K2_TO*n_wait_events)
            # Probabilities are computed as (k1 * (current_iter - last_iter) + k2)
            # k1 is a fixed term for each node
            # k2 is a percentage of the number of nodes and changes on each iteration
            pr = np.multiply(k1_values, np.subtract(iteration_counter, last_iteration))
            probabilities = np.add(pr, k2_values)
            # Minimum probability
            this_iteration = np.argmax(probabilities)
            logging.debug(f"Iteration {iteration_counter}: execute {this_iteration}")

            # Execute this_iteration
            self.__wait_events[this_iteration].set()
            # Save last iteration to compute probabilities
            last_iteration[this_iteration] = iteration_counter

            # Wait for all ack events from time to time
            if CustomScheduler.WAIT_ALL != 0 and \
                    iteration_counter % (n_wait_events*CustomScheduler.WAIT_ALL) == 0:
                for i, event in enumerate(self.__ack_events):
                    if self.__end_event.is_set():
                        break
                    logging.debug(f"Wait for acknowledge {i+1} of {len(self.__ack_events)}")
                    event.wait()
                    event.clear()

        if not self.__num_iter is None:
            self.__end_event.set()

        # Generate events to unlock loops
        for i, event in enumerate(self.__wait_events):
            logging.debug(f"Set wait event {i+1} of {n_wait_events}")
            event.set()
            # Wait for acknowledge
            self.__ack_events[i].wait()

        statistics.finished()
        logging.debug("Finishing scheduler")
