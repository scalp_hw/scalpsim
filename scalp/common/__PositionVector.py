
from typing import NamedTuple


class PositionVector(NamedTuple):
    """
    Vector symbolising a node position in the the 3D SCALP network
    """
    x: int
    y: int
    z: int