from typing import Tuple, Dict, List, Optional, Type, ClassVar
from threading import Thread, Event

from scalp.common import Direction, PositionVector, Message, thread_yield
from scalp.communication import PhysicalNode
from scalp import statistics
from scalp.router import Router, RouteResult
from scalp.router.multicast import BroadcastRouter, ReduceRouter
from scalp.perf.Clock import Clock
from time import sleep


class NoCNode:
    """
    Represents the FPGA in the SCALP architecture.
    It mainly executes the routing operations, that it delegates to the diferent routers.
    """
    MAX_WAIT_TIME: ClassVar[float] = 1

    def __init__(
        self,
        position: PositionVector,
        network_size: PositionVector,
        physicalnode: PhysicalNode,
        start_event: Event,
        end_event: Event,
        router_type: Type[Router],
        computingcore_type: Type["ComputingCore"],
        clock: Clock,
        events_tuple: Tuple[Event, Event, Event],
    ) -> None:
        super().__init__()

        self.__position = position
        self.__physicalnode = physicalnode
        self.__clock = clock
        self._end_event = end_event
        self.__routers : List[Router] = [
            router_type(physicalnode, position),
            BroadcastRouter(physicalnode, position),
            ReduceRouter(physicalnode, position),
        ]

        self._wait_event = events_tuple[0]
        self._ack_event = events_tuple[2]

        self.__thread = Thread(target=self.__run, name=f"NoCNode {position}")
        self.__thread.daemon = True
        self.__thread.start()

        self.__computingcore = computingcore_type(
            node=self, network_size=network_size, start_event=start_event, end_event=end_event,
            wait_event=events_tuple[1], ack_event=events_tuple[3]
        )

    @property
    def physicalnode(self) -> PhysicalNode:
        return self.__physicalnode

    @property
    def position(self) -> PositionVector:
        return self.__position

    @property
    def computingcore(self) -> "ComputingCore":
        return self.__computingcore

    @property
    def clock(self) -> Clock:
        return self.__clock

    def send_msg_to_fpga(self, msg: Message) -> bool:
        if self.physicalnode.is_full(Direction.FPGA):
            return False

        msg.stats.sent_time = self.__clock.time
        self.physicalnode.send(Direction.FPGA, msg)
        statistics.injected_msg(self.__position, self.clock.time, msg)
        return True

    def __run(self) -> None:
        try:
            while not self._end_event.is_set():
                self._wait_event.wait()
                self._wait_event.clear()
                if self._end_event.is_set():
                    # Last iteration
                    self._ack_event.set()
                    return

                route_res = RouteResult.NO_MSG_TO_ROUTE
                for router in self.__routers:
                    res = router.perform_route()
                    route_res = route_res.merge(res)
                    if route_res == RouteResult.MSG_ROUTED:
                        break
                self.__handle_internal_msgs()
                if route_res == RouteResult.FULL_OUTGOING_CHANNELS:
                    statistics.interface_full(self.__position)
                    thread_yield()
                self.__clock.route_done()
                self._ack_event.set()
        except:
            statistics.error()
            raise

    def __handle_internal_msgs(self):
        while not self.__physicalnode.is_empty(Direction.ComputingCore):
            msg = self.__physicalnode.in_channels[Direction.ComputingCore].pop()
            self.__computingcore.handle_internal_msg(msg)

    def __str__(self):
        return str(self.__position)

