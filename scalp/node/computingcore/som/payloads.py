from typing import Tuple
from scalp.common import PositionVector


class InputVectorPayload:
    def __init__(self, input_vector: Tuple[float, ...]):
        self.input_vector = input_vector

class InputLengthPayload:
    def __init__(self, length: int):
        self.length = length

class BMUPayload:
    def __init__(self, input_vector: Tuple[float, ...], bmu_position: PositionVector, t:int):
        self.input_vector = input_vector
        self.bmu_position = bmu_position
        self.t = t
