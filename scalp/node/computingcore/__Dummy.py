from threading import Event
from scalp.common import PositionVector, Message, YIELD_EPSILON, Direction
from scalp import statistics
from scalp.node import NoCNode
from .__ComputingCore import ComputingCore

class Dummy(ComputingCore):
    """
    Dummy computing core. It does no inject new messages into the network.
    """
    def _execute(self) -> None:
        pass
