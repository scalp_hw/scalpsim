from typing import ClassVar, Union
from threading import Event
from time import sleep
from functools import reduce
from scalp import statistics
from scalp.common import (
    PositionVector,
    UnicastMessage,
    BroadcastMessage,
    ReduceMessage,
    ReduceResponseMessage,
)
from scalp.node import NoCNode
from scalp.node.computingcore import ComputingCore
import numpy as np
import scalp.node.distribution as distribution


class GenerateBroadcastMessages(ComputingCore):
    """
    Generates a specified number of broadcast messages.
    The purpose of this computing core is to test the broadcast algorithm and make sure it is dead lock free.
    """
    MSGS_2_GENERATE: ClassVar[int] = 5
    finished_computingcores: ClassVar[int] = 0

    def __init__(
        self, node: NoCNode, network_size: PositionVector, start_event: Event, end_event: Event,
        wait_event: Event, ack_event: Event
    ) -> None:
        super().__init__(node, network_size, start_event, end_event, wait_event, ack_event)
        self._node_count = reduce(lambda a, c: a * c, network_size)
        self._expected_msgs = self.MSGS_2_GENERATE * self._node_count

    def _execute(self) -> None:
        for i in range(self.MSGS_2_GENERATE):
            msg = BroadcastMessage(
                source=self._node.position, payload=f"{i}@{self._node.position}"
            )

            self.send_to_fpga(msg)

    def handle_broadcast_msg(self, msg: BroadcastMessage)->None:
        """
        Handle broadcast messages received by ComputingCore
        """
        self._expected_msgs -= 1
        if self._expected_msgs == 0:
            GenerateBroadcastMessages.finished_computingcores += 1
            if self._node_count == GenerateBroadcastMessages.finished_computingcores:
                print(f"Finished ComputingCoreS: {GenerateBroadcastMessages.finished_computingcores}")
