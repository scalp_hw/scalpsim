from typing import ClassVar, Union
from threading import Event
from time import sleep
import numpy as np
from scalp.common import PositionVector, UnicastMessage, BroadcastMessage, ReduceMessage, ReduceResponseMessage
from scalp.node import NoCNode, distribution
from scalp import statistics
from scalp.node.computingcore import ComputingCore


class GenerateUnicastMessages(ComputingCore):
    """
    Generates unicast messages.
    Messages are generated at a given interval, and destinations are chosen
    according to a specified distribution strategy.
    The purpose of this computing core is to stress the network and comparing distinct unicast routing algorithms.
    """
    INTERVAL: ClassVar[int] = 1
    DISTRIBUTION: ClassVar[distribution.distribution] = distribution.complement

    def __init__(
        self, node: NoCNode, network_size: PositionVector, start_event: Event, end_event: Event,
        wait_event: Event, ack_event: Event
    ) -> None:
        super().__init__(node, network_size, start_event, end_event, wait_event, ack_event)

    def __pick_target_position(self):
        return GenerateUnicastMessages.DISTRIBUTION(
            source=self.node.position, network_size=self._network_size, rng=self._rng
        )

    def _execute(self) -> None:
        iteration = 0
        last_exec = 0
        next_iter = self._rng.randint(1, self.INTERVAL)

        while not self._end_event.is_set():
            self._wait_event.wait()
            self._wait_event.clear()
            if self._end_event.is_set():
                # Last iteration
                self._ack_event.set()
                return

            if (iteration-last_exec) >= next_iter:
                target = self.__pick_target_position()
                msg = UnicastMessage(
                    source=self.node.position,
                    target=target,
                    payload=f"{self.node.position}",
                )
                self.send_to_fpga(msg)

                # Update last_exec and next_iter
                last_exec = iteration
                next_iter = self._rng.randint(1, self.INTERVAL)

            self._ack_event.set()
            iteration += 1

    def handle_unicast_msg(self, msg: UnicastMessage) -> None:
        """
        Handle messages received by ComputingCore
        """
        # statistics.received(self.node.position, self.node.clock.time, msg)
        pass
