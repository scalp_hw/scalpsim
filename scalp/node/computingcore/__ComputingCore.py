from abc import ABC, abstractmethod
from threading import Thread, Event
from typing import Tuple, Deque, Union
from collections import deque
from weakref import ref

from scalp import random, statistics
from scalp.common import (
    PositionVector,
    Message,
    BroadcastMessage,
    UnicastMessage,
    ReduceMessage,
    ReduceResponseMessage,
    thread_yield,
)
from scalp.node import NoCNode


class ComputingCore(ABC):
    """
    Base class for the computing core. A
    A computing core represents an algorithm that is being executed by the CPU architecture of a node.
    """
    def __init__(
        self, node: NoCNode, network_size: PositionVector, start_event: Event, end_event: Event,
        wait_event: Event, ack_event: Event
    ) -> None:
        self.__node = node
        self._network_size = network_size

        unique_computingcore_seed = (
            node.position.x * network_size.y + node.position.y
        ) * network_size.z + node.position.z
        self._rng = random.create_random_state(seed=unique_computingcore_seed)
        self.__start_event = start_event
        self._end_event = end_event
        self._wait_event = wait_event
        self._ack_event = ack_event
        self.__msgs_to_fpga: Deque[Message] = deque()
        self.__msgs_to_fpga_event = Event()

        thread = Thread(target=self.__run, name=f"ComputingCore@{node.position}")
        thread.daemon = True
        thread.start()

        thread = Thread(target=self.__flush_fpga_msgs, name=f"ComputingCore2FPGA@{node.position}")
        thread.daemon = True
        thread.start()

    @property
    def node(self):
        return self.__node

    def __run(self) -> None:

        self.__start_event.wait()

        statistics.started()

        try:
            self._execute()
        except:
            statistics.error()
            raise
        statistics.finished(self.node.position, self.node.clock.time)

    def __flush_fpga_msgs(self) -> None:
        while not self._end_event.is_set():
            self.__msgs_to_fpga_event.wait(2)
            self.__msgs_to_fpga_event.clear()
            while self.__msgs_to_fpga and not self._end_event.is_set():
                msg = self.__msgs_to_fpga[-1]
                if self.node.send_msg_to_fpga(msg):
                    self.__msgs_to_fpga.pop()
                else:
                    thread_yield()

    def send_to_fpga(self, msg: Message) -> None:
        self.__msgs_to_fpga.appendleft(msg)
        self.__msgs_to_fpga_event.set()

    @abstractmethod
    def _execute(self) -> None:
        """
        ComputingCore behavior goes here
        """
        pass

    def handle_internal_msg(self, msg) -> None:
        """
        Handle messages received by ComputingCore
        """
        if isinstance(msg, UnicastMessage):
            self.handle_unicast_msg(msg)
        elif isinstance(msg, ReduceMessage):
            result = self.handle_reduce_msg(msg)
            response = msg.create_response(value=result, source=self.node.position)
            self.send_to_fpga(response)
        elif isinstance(msg, ReduceResponseMessage):
            self.handle_reduce_response_msg(msg)
        elif isinstance(msg, BroadcastMessage):
            self.handle_broadcast_msg(msg)
        else:
            raise Exception(f"Unknown message type{type(msg).__name__}")

        statistics.received(self.node.position, self.node.clock.time, msg)

    def handle_broadcast_msg(self, msg: BroadcastMessage) -> None:
        """
        Handle received broadcast messages
        """
        raise Exception("Not implemented")

    def handle_unicast_msg(self, msg: UnicastMessage) -> None:
        """
        Handle received unicast messages
        """
        raise Exception("Not implemented")

    def handle_reduce_msg(self, msg: ReduceMessage) -> Union[int, float]:
        """
        Handle received reduce messages
        """
        raise Exception("Not implemented")

    def handle_reduce_response_msg(self, msg: ReduceResponseMessage) -> None:
        """
        Handle received reduce response messages
        """
        raise Exception("Not implemented")
