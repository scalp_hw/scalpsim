"""
The statistics module agreggates performance and debugging events.

The module keeps track of the components that are running,
the messages that have been exchanged, the latency statistics.
"""
import traceback
import logging
from functools import reduce
from itertools import product
from typing import Optional, Union, List, Dict
from collections import defaultdict
from threading import Lock, Condition
from datetime import datetime
from dataclasses import dataclass, field
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from scalp.common import Direction, PositionVector, Message

__logger = logging.getLogger(__name__)


class _NoCNodesStats:
    def __init__(self):
        self.__stats = defaultdict(lambda: defaultdict(int))

    def add_stat(self, node_pos: PositionVector, stat_type: str):
        self.__stats[node_pos][stat_type] += 1

    def get_stats(self, stat_type: str):
        return {
            pos: self.__stats[pos][stat_type]
            for pos in self.__stats
            if stat_type in self.__stats[pos]
        }


@dataclass
class __SimStats:
    queues_sum: List[int]
    queues_times: List[int]
    start_time: datetime
    lock = Lock()
    end_condition = Condition()
    node_stats = _NoCNodesStats()
    size: Optional[PositionVector] = None
    msg_sent: int = 0
    msg_received: int = 0
    full_events: int = 0
    running_nodes: int = 0
    total_hops_count: int = 0
    total_transport_time: int = 0
    max_hop_time: int = 0
    max_received_time: int = 0
    error_occured: Optional[str] = None
    var_m = var_s = 0  # Welford’s variance method
    final_times: Dict[PositionVector, int] = field(default_factory=dict)

    # queues_sizes: Dict[PositionVector, Dict[Direction, int]] = defaultdict(lambda x: defaultdict(int))

    def __init__(self):
        self.queues_sum = [0]
        self.queues_times = [0]
        self.start_time = datetime.now()
        self.final_times = defaultdict(int)

    def hops_variance(self) -> Union[float, "NA"]:
        return self.var_s / (self.msg_sent - 1) if self.msg_sent > 1 else "NA"

    def update_hop_time_variance(self, transport_time: int, hops: int) -> None:
        if hops > 0:
            hop_time = transport_time / hops
            m = self.var_m
            self.var_m += (hop_time - self.var_m) / self.msg_received
            self.var_s += (hop_time - self.var_m) * (hop_time - m)

    def queue_add(self, time):
        self.queues_sum.append(self.queues_sum[-1] + 1)
        self.queues_times.append((datetime.now() - self.start_time).total_seconds())

    def queue_rem(self, time):
        self.queues_sum.append(self.queues_sum[-1] - 1)
        self.queues_times.append((datetime.now() - self.start_time).total_seconds())


__simstats = __SimStats()


def reset_simulation(size: PositionVector):
    __simstats.size = size
    __simstats.start_time = datetime.now()


def injected_msg(node_pos: PositionVector, time: int, msg: Message):
    __simstats.node_stats.add_stat(node_pos, "injected")

    with __simstats.lock:
        __simstats.msg_sent += 1
        __simstats.queue_add(time)

    __logger.debug(f"Injected msg, travelling {__simstats.msg_sent - __simstats.msg_received}")
    __logger.debug(f"{node_pos} sent {msg}")


def received(node_pos: PositionVector, time: int, msg: Message):
    __simstats.node_stats.add_stat(node_pos, "received")

    with __simstats.lock:
        __simstats.msg_received += 1
        __simstats.total_hops_count += msg.stats.hops
        __simstats.total_transport_time += msg.stats.transport_time
        __simstats.max_hop_time = max(__simstats.max_hop_time, msg.stats.max_hop_time)
        # __simstats.max_received_time = max(
        #     __simstats.max_received_time, msg.stats.transport_time + msg.stats.sent_time
        # )
        __simstats.update_hop_time_variance(msg.stats.transport_time, msg.stats.hops)
        __simstats.queue_rem(time)

    if (
        __simstats.msg_received == __simstats.msg_sent
    ):  # all sent messages were received
        with __simstats.end_condition:
            __simstats.end_condition.notifyAll()

    __logger.debug(f"Received msg, travelling {__simstats.msg_sent - __simstats.msg_received}")
    __logger.debug(f"{node_pos} received {msg}")


def routed(node_pos: PositionVector):
    __simstats.node_stats.add_stat(node_pos, "routed")

    __logger.debug(f"{node_pos} routed a message")


def interface_full(node_pos: PositionVector):
    __simstats.node_stats.add_stat(node_pos, "iffull")
    with __simstats.lock:
        __simstats.full_events += 1

    __logger.debug(f"{node_pos} Route operation failed full")


def started():
    with __simstats.lock:
        __simstats.running_nodes += 1


def finished(position: PositionVector = None, final_time: int = None):
    with __simstats.lock:
        __simstats.running_nodes -= 1
        if position and final_time:
            __simstats.final_times[position] = final_time

    if __simstats.running_nodes == 0:
        with __simstats.end_condition:
            __simstats.end_condition.notifyAll()


def wait_end(timeout: int = None) -> bool:
    """
    Waits until the simulation ends.
    :returns: True if the simulation ended correctly, False otherwise
    """
    with __simstats.end_condition:
        # res = __simstats.end_condition.wait_for(
        #     predicate=lambda: __simstats.error_occured is not None
        #     or __simstats.msg_received == __simstats.msg_sent
        #     and __simstats.running_nodes == 0,
        #     timeout=timeout,
        # )
        res = __simstats.end_condition.wait_for(
            predicate=lambda: __simstats.running_nodes == 0,
            timeout=timeout,
        )
        if res == False:
            __simstats.error_occured = f"Timeout occured. Running_nodes: {__simstats.running_nodes}"
            return res

    return not __simstats.error_occured


def error():
    __simstats.error_occured = traceback.format_exc()
    with __simstats.end_condition:
        __simstats.end_condition.notifyAll()


def print_error():
    __logger.info(f"\nError: {__simstats.error_occured}")


def get_stats():
    seconds_elapsed = (datetime.now() - __simstats.start_time).total_seconds()
    avg_hop_time = 0
    msg_per_cycle = 0
    # Compute max_received_time with final_times
    __simstats.max_received_time = max(__simstats.final_times.values())
    if __simstats.max_received_time > 0 and __simstats.total_hops_count > 0:
        avg_hop_time = __simstats.total_transport_time / __simstats.total_hops_count
        msg_per_cycle = __simstats.msg_received / __simstats.max_received_time

    return {
        "avg_throughput_cycle": msg_per_cycle,
        "avg_latency": avg_hop_time,
        "max_latency": __simstats.max_hop_time,
        "latency_variance": __simstats.hops_variance(),
        "total_hops": __simstats.total_hops_count,
        "msgs_sent": __simstats.msg_sent,
        "msgs_recv": __simstats.msg_received,
        "cycles": __simstats.max_received_time,
        "full_events": __simstats.full_events,
        "elapsed": seconds_elapsed,
    }


def print_stats():
    stats = get_stats()
    print(
        f"""Simulation finished.
Average throughput:\t{stats['avg_throughput_cycle']: > 13.2f}
Avg hop time: \t\t{stats['avg_latency']: > 13.2f}
Peak hop time: \t\t{stats['max_latency']: > 10d}
Std hop time: \t\t{stats['latency_variance']: > 13.2f}
Total hops: \t\t{stats['total_hops']: > 10d}
Sent msgs: \t\t{stats['msgs_sent']: > 10d}
Received msgs: \t\t{stats['msgs_recv']: > 10d}
Elapsed cycles: \t{stats['cycles']: > 10d}
IF full events: \t{stats['full_events']: > 10d}
Simulation time(s): \t{stats['elapsed']: > 13.2f} s
    """
    )


def print_non_empty_queues(network):
    """
    Debugging method
    """
    for _, node in network.nodes.items():
        for direction, channel in node._NoCNode__physicalnode.in_channels.items():
            if channel.size() > 0:
                print(node, direction, channel.size(), channel.peek())


def print_queues_state():
    plt.plot(__simstats.queues_times, __simstats.queues_sum)
    plt.show(block=True)


def display_received_stats():
    node_received = __simstats.node_stats.get_stats("iffull")
    if node_received:
        xs, ys, zs = zip(*[(val.x, val.y, val.z) for val in node_received.keys()])
        values = list(node_received.values())
        fig = plt.figure()

        ax = fig.add_subplot(111, projection="3d")
        plot_idx = [i for (i, v) in enumerate(values) if v > 0]
        plot = ax.scatter(
            xs=[xs[i] for i in plot_idx],
            ys=[ys[i] for i in plot_idx],
            zs=[zs[i] for i in plot_idx],
            c=[values[i] for i in plot_idx],
            cmap="plasma",
            alpha=0.6,
            s=40,
        )
        fig.colorbar(plot)
        plt.title("Full events")

    routed_msg = __simstats.node_stats.get_stats("routed")
    if routed_msg:
        xs, ys, zs = zip(*[(val.x, val.y, val.z) for val in routed_msg.keys()])
        values = list(routed_msg.values())
        fig = plt.figure()

        ax = fig.add_subplot(111, projection="3d")
        plot_idx = [i for (i, v) in enumerate(values) if v > 0]
        plot = ax.scatter(
            xs=[xs[i] for i in plot_idx],
            ys=[ys[i] for i in plot_idx],
            zs=[zs[i] for i in plot_idx],
            c=[values[i] for i in plot_idx],
            cmap="plasma",
            alpha=0.6,
            s=40,
        )
        fig.colorbar(plot)
        plt.title("Sent messages")


    # dir_space = 0.05
    # points_from_dir = {
    #     Direction.UP: lambda p: ([p[0]] * 2, [p[1] + dir_space] * 2, [p[2], p[2] + 1]),
    #     Direction.DOWN: lambda p: (
    #         [p[0]] * 2,
    #         [p[1] - dir_space] * 2,
    #         [p[2], p[2] - 1],
    #     ),
    #     Direction.NORTH: lambda p: (
    #         [p[0]] * 2,
    #         [p[1], p[1] + 1],
    #         [p[2] + dir_space] * 2,
    #     ),
    #     Direction.SOUTH: lambda p: (
    #         [p[0]] * 2,
    #         [p[1], p[1] - 1],
    #         [p[2] - dir_space] * 2,
    #     ),
    #     Direction.WEST: lambda p: (
    #         [p[0], p[0] - 1],
    #         [p[1]] * 2,
    #         [p[2] + dir_space] * 2,
    #     ),
    #     Direction.EAST: lambda p: (
    #         [p[0], p[0] + 1],
    #         [p[1]] * 2,
    #         [p[2] - dir_space] * 2,
    #     ),
    # }

    # cmap = plt.get_cmap("coolwarm")
    # max_routes_dir = 0
    # for direction in Direction:
    #     routed_dir = __simstats.node_stats.get_stats(f"routed_{direction}")
    #     print(len(routed_dir))
    #     max_routes_dir = max(max_routes_dir, max(routed_dir.values()))

    # for direction in Direction:
    #     routed_dir = __simstats.node_stats.get_stats(f"routed_{direction}")
    #     positions = list(routed_dir.keys())
    #     for pos in positions:
    #         v = routed_dir[pos]
    #         if not v:
    #             continue
    #         line = points_from_dir[direction](pos)
    #         value_norm = v / max_routes_dir
    #         ax.plot(
    #             *line, c=cmap(value_norm), alpha=0.4 + value_norm * 0.3, linewidth=1
    #         )
    plt.show(block=True)

def display_final_stats(network_size: PositionVector):
    # Compute maximum elapsed time
    max_time = max(__simstats.final_times.values())

    def try_append_value(append_list: list, stat_dict: dict, pos: PositionVector):
        """Append value to list or zero if KeyError"""
        try:
            append_list.append(stat_dict[pos])
        except KeyError:
            append_list.append(0)

    final_times = list()
    msg_recv = list()
    msg_inj = list()
    msg_routed = list()
    labels = list()
    for pos in product(*[range(d) for d in network_size]):
        final_times.append(__simstats.final_times[PositionVector(*pos)]/float(max_time))
        try_append_value(msg_inj, __simstats.node_stats.get_stats("injected"), pos)
        try_append_value(msg_recv, __simstats.node_stats.get_stats("received"), pos)
        try_append_value(msg_routed, __simstats.node_stats.get_stats("routed"), pos)
        labels.append(str(pos))

    # Fig 1: Final times
    fig = plt.figure()
    plt.plot(final_times)
    plt.title(f"Final time (max: {max_time})")
    plt.xlabel("Position in the grid")
    plt.ylabel("Normalized final time")
    # X labels: Positions in the grid
    plt.xticks(range(len(labels)), labels, rotation='vertical')

    # Fig 2: Number of messages sent/received
    fig = plt.figure()
    plt.plot(msg_inj, 'k', label="Injected messages")
    plt.plot(msg_recv, 'b', label="Received messages")
    plt.plot(msg_routed, 'r', label="Routed messages")
    plt.title(f"Message counters")
    plt.xlabel("Position in the grid")
    plt.ylabel("Number of messages")
    # X labels: Positions in the grid
    plt.xticks(range(len(labels)), labels, rotation='vertical')
    plt.legend()

    # Show plots
    plt.show(block=True)
