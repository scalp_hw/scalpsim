from itertools import product
from typing import List, Dict, Tuple, Optional, Iterable, Type
from threading import Event
from functools import reduce
from time import sleep
from scalp import random
from scalp.node import NoCNode
from scalp.common import (
    PositionVector,
    CustomScheduler,
    directions_between_neighbours,
    neighbour_positions,
)
from scalp.communication import PhysicalNode
from scalp.perf.Clock import Clock
from scalp.router import Router, unicast
from scalp.node.computingcore import ComputingCore
from scalp import statistics


class Network:
    """
    Network build a SCALP network with dimensions 'size'.
    Each node will have a computing cores of type 'computingcore_type'.
    The routing algorithm is specified in 'router_type'.

    The 'simulate' function starts the simulation
    and 'wait_end' waits until every single component has finished.
    """

    def __init__(
        self,
        size: PositionVector,
        computingcore_type: Type[ComputingCore],
        router_type: Type[Router] = unicast.XyzRouter,
        num_cycles: int = None,
    ) -> None:
        self.__size = size
        self.__num_nodes = reduce(lambda a, c: a * c, size)
        self.__rng = random.create_random_state()
        self.__unicast_router_type = router_type
        self.__computingcore_type = computingcore_type
        self.__start_event = Event()
        self._end_event = Event()
        self.__num_cycles = num_cycles
        self.__build_network()

    @property
    def size(self) -> PositionVector:
        """
        The network size
        """
        return self.__size

    @property
    def num_nodes(self) -> int:
        """
        Number of nodes in the network
        """
        return self.__num_nodes

    @property
    def nodes(self) -> Dict[PositionVector, NoCNode]:
        """
        The network nodes
        """
        return self.__nodes

    @property
    def start_event(self) -> Event:
        """
        Event triggered when the smiulation starts
        """
        return self.__start_event

    def __build_network(self) -> None:
        self.__nodes_physicalnode: Dict[PositionVector, Tuple[PhysicalNode, Clock]] = {}
        # Wait and ack events (NoCNode and ComputingCore) for each node
        self.__nodes_events: Dict[PositionVector, Tuple[Event, Event, Event, Event]] = {}
        for pos in product(*[range(d) for d in self.__size]):
            position = PositionVector(*pos)
            clock = Clock()
            self.__nodes_physicalnode[position] = (PhysicalNode(position, clock), clock)
            self.__nodes_events[position] = (Event(), Event(), Event(), Event())

        for pos, (phys, clock) in self.__nodes_physicalnode.items():
            neighbours = self.get_neighbours(pos)
            for neighbour_pos in neighbours:
                directions = directions_between_neighbours(pos, neighbour_pos)
                assert (
                    directions is not None
                ), "Cannot find direction between neighbours"
                diemention, send_dir, received_dir = directions
                neighbour_channel = self.__nodes_physicalnode[neighbour_pos][0].in_channels[
                    received_dir
                ]
                phys.attach_neighbour_channel(send_dir, neighbour_channel)

        self.__nodes: Dict[PositionVector, NoCNode] = {}

        for pos, (physicalnode, clock) in self.__nodes_physicalnode.items():
            self.__nodes[pos] = NoCNode(
                position=pos,
                network_size=self.__size,
                physicalnode=physicalnode,
                clock=clock,
                start_event=self.__start_event,
                end_event=self._end_event,
                router_type=self.__unicast_router_type,
                computingcore_type=self.__computingcore_type,
                events_tuple=self.__nodes_events[pos]
            )
        # Set number of cycles: number of iterations = cycles * nodes
        if self.__num_cycles is None:
            num_iter = None
        else:
            num_iter = self.__num_cycles * self.__num_nodes
        # Scheduler
        self.__nocnode_sched = CustomScheduler(self.__start_event, self._end_event,
                                               [ev[0] for ev in self.__nodes_events.values()] + \
                                                   [ev[1] for ev in self.__nodes_events.values()],
                                               [ev[2] for ev in self.__nodes_events.values()] + \
                                                   [ev[3] for ev in self.__nodes_events.values()],
                                               self.__rng, num_iter)

    def get_neighbours(self, pos: PositionVector) -> Iterable[PositionVector]:
        """
        Returns the node's neighbors positions
        """
        for possible_pos in neighbour_positions(pos):
            if possible_pos in self.__nodes_physicalnode:
                yield possible_pos

    def simulate(self) -> None:
        """
        Starts the simulations
        """
        statistics.reset_simulation(size=self.__size)
        statistics.started()

        self.__start_event.set()  # awakes those waiting for the start event
        sleep(0.1)

    def wait_end(self, timeout: int = None) -> bool:
        """
        Waits until all the simulation components have finished
        """
        statistics.finished()
        return statistics.wait_end(timeout)
